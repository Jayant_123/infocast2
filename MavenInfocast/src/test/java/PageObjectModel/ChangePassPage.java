package PageObjectModel;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import stepDefinitions.Hooks;
import utility.ConfigFileReader;

public class ChangePassPage extends Hooks {


	public static WebElement element = null;
	@FindBy(xpath="//a[text()='My Setting']")
	public static WebElement mysettings;

	@FindBy(id="changePassword")
	public static WebElement changepass;

	@FindBy(id="changePasswordForm:j_id18")
	public static WebElement oldpass;


	@FindBy(id="changePasswordForm:j_id20")
	public static WebElement newpass;


	// my settings button
	public static WebElement mySettings() {
		element = BusinessFunctions.getWebDriver().findElement(By.xpath("//a[text()='My Setting']"));
		return element;
	}

	//ChangePassword Button
	public static WebElement changePassword() {
		element = BusinessFunctions.getWebDriver().findElement(By.id("changePassword"));
		return element;
	}

	//select old password
	public static WebElement oldPassword() {
		element = BusinessFunctions.getWebDriver().findElement(By.id("changePasswordForm:j_id18"));
		return element;
	}

	//select new password
	public static WebElement newPassword() {
		element = BusinessFunctions.getWebDriver().findElement(By.id("changePasswordForm:j_id20"));
		return element;
	}

	//select confirm password
	public static WebElement confirmPassword() {
		element = BusinessFunctions.getWebDriver().findElement(By.id("changePasswordForm:j_id22"));
		return element;
	}

	//select save button
	public static WebElement passwordSave() {
		element = BusinessFunctions.getWebDriver().findElement(By.id("changePasswordForm:j_id25"));
		return element;
	}

	//chnge password page
	public static WebElement changePPage() {
		element = BusinessFunctions.getWebDriver().findElement(By.xpath("//div[text()='Change Password']"));
		return element;
	}

	//error message
	public static WebElement errorMessage() {
		element = BusinessFunctions.getWebDriver().findElement(By.className("iceMsgsInfo"));
		return element;
	}

	//sign out
	public static WebElement signOut() {
		element = BusinessFunctions.getWebDriver().findElement(By.id("portalSignOut"));
		return element;
	}
}
	/*WebDriver driver;
	ConfigFileReader config;
	WebDriverWait wait;

	@FindBy(xpath="//a[text()='My Setting']")
	WebElement mysettings;

	@FindBy(id="changePassword")
	WebElement changepass;

	@FindBy(id="changePasswordForm:j_id18")
	WebElement oldpass;


	@FindBy(id="changePasswordForm:j_id20")
	WebElement newpass;

	@FindBy(id="changePasswordForm:j_id22")
	WebElement confirmpass;

	@FindBy(id="changePasswordForm:j_id25")
	WebElement save;

	@FindBy(className="iceMsgsInfo")
	WebElement mess;


	public ChangePassPage(WebDriver driver)
	{
		this.driver = driver;

        //This initElements method will create all WebElements

        PageFactory.initElements(driver, this);
        ConfigFileReader configFileReader = new ConfigFileReader();
        wait= new WebDriverWait(driver,30);
	}

public void clickMySettings() throws InterruptedException {

		Thread.sleep(3000);
		wait.until(ExpectedConditions.visibilityOf(mysettings)).click();
	}

	public void toChangePass() {

		wait.until(ExpectedConditions.visibilityOf(changepass)).click();


	}

	public void setoldPass(String old) {
		wait.until(ExpectedConditions.visibilityOf(oldpass)).sendKeys(old);

	}

	public void setnewPass(String neww) {
		wait.until(ExpectedConditions.visibilityOf(newpass)).sendKeys(neww);

	}

	public void setconPass(String confirm) {
		wait.until(ExpectedConditions.visibilityOf(confirmpass)).sendKeys(confirm);

	}
	public void toSave()

	{
		wait.until(ExpectedConditions.visibilityOf(save)).click();

	}
	}
*/