package PageObjectModel;


import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import stepDefinitions.Hooks;


public class LoginPF extends Hooks {



	public static WebElement element = null;

	// username
    public static WebElement username() {
		element = BusinessFunctions.getWebDriver().findElement(By.name("username"));
		return element;
	}
	// password
	public static WebElement password() {
		element = BusinessFunctions.getWebDriver().findElement(By.name("password"));
		return element;
	}
	// login submit
	public static WebElement submit() {
		element = BusinessFunctions.getWebDriver().findElement(By.id("btnSubmit"));
		return element;
	}
	// agree button
	public static WebElement agree() {
		element = BusinessFunctions.getWebDriver().findElement(By.id("btnOK"));
		return element;
	}
	// notagree button
	public static WebElement decline() {
		element = BusinessFunctions.getWebDriver().findElement(By.id("btnCancel"));
		return element;
	}
	// loginStatus
	public static WebElement loginStatus() {
		element = BusinessFunctions.getWebDriver().findElement(By.id("status"));
		return element;
	}
    public static WebElement signOut() {
        element = BusinessFunctions.getWebDriver().findElement(By.id("portalSignOut"));
        return element;
    }

}
