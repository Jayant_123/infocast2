package PageObjectModel;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;

public class PerformAction {

	static WebDriver driver;
	
	/*
	'================================================================================================================
	' FUNCTION NAME	: fn_WebEdit()
	' PURPOSE		: This function Set the value for an Web Edit box.
	' PARAMETERS	: in string: val - Value to Insert in Web Edit box.
					in string: WE - Web Element identifer (This is be declare in the POM page with proper locator).
	  Authour		: Jayant S
	  Reviewer		: Venkata Kumar B
	' EXAMPLE		: fn_WebEdit(WebElement WE, Value val)
	'=================================================================================================================
	
*/	
	//LAUNCH BROWSER
	public static WebDriver fn_LaunchBrowser(String browsername){
		
		if(browsername=="Chrome"){
		System.setProperty("webdriver.chrome.driver",System.getProperty("user.dir")+"\\lib\\chromeP\\chromedriver.exe");
		driver= new ChromeDriver();
		}else if(browsername=="Firefox"){
		System.setProperty("webdriver.gecko.driver",System.getProperty("user.dir")+"\\lib\\geckoJars\\geckodriver.exe");	
		driver= new FirefoxDriver();
		}else if(browsername=="Internet Explorer"){
		System.setProperty("webdriver.gecko.driver",System.getProperty("user.dir")+"\\lib\\IEDriverServer_x64_3.14.0\\IEDriverServer.exe");
		driver= new InternetExplorerDriver();
		}
		driver.manage().timeouts().implicitlyWait(80, TimeUnit.SECONDS);
		return driver;
		}
	
	//OPEN APPLICATION
	public static WebDriver fn_OpenApp(String BrowserName, String url){
		fn_LaunchBrowser(BrowserName);
		fn_OpenURL(url);
		return driver;
		}
	
	//OPEN URL
	public static void fn_OpenURL(String url){
		driver.get(url);
		driver.manage().window().maximize();
		}
	
	//CLICK ACTION
	public static void fn_Click(WebElement WE) {
		
		WE.click();
	}
}
