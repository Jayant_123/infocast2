package PageObjectModel;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;

import stepDefinitions.Hooks;
import utility.ConfigFileReader;

public class HomePage extends Hooks {

	
/*
	WebDriver driver;
	ConfigFileReader config;
	WebDriverWait wait;*/


	public static WebElement element = null;

	public static WebElement buy() {
		element = BusinessFunctions.getWebDriver().findElement(By.xpath(".//*[@id='portlet:mainForm:j_id32']"));
		return element;
	}

	public static WebElement sell() {
		element = BusinessFunctions.getWebDriver().findElement(By.id("btn_sell\""));
		return element;
	}

	public static WebElement market() {
		element = BusinessFunctions.getWebDriver().findElement(By.id("portlet:mainForm:market"));
		return element;
	}

	public static WebElement stockCode() {
		element = BusinessFunctions.getWebDriver().findElement(By.id("portlet:mainForm:symbol"));
		return element;
	}

	public static WebElement orderPrice() {
		element = BusinessFunctions.getWebDriver().findElement(By.id("portlet:mainForm:limitPrice"));
		return element;
	}


	public static WebElement orderQuantity() {
		element = BusinessFunctions.getWebDriver().findElement(By.id("portlet:mainForm:orderQty"));
		return element;
	}

	public static WebElement previewOrder() {
		element = BusinessFunctions.getWebDriver().findElement(By.id("portlet:mainForm:button_previewOrder"));
		return element;
	}

	public static WebElement mySettings() {
		element = BusinessFunctions.getWebDriver().findElement(By.xpath("//a[text()='My Setting']"));
		return element;
	}

	public static WebElement homeTitle() {
		element = BusinessFunctions.getWebDriver().findElement(By.xpath("//title[text()='InvestTrade iWeb']"));
		return element;
	}

	public static WebElement orders() {
		element = BusinessFunctions.getWebDriver().findElement(By.tagName("Orders"));
		return element;
	}

	public static WebElement trade() {
		element = BusinessFunctions.getWebDriver().findElement(By.xpath("//a[text()='Trade']"));
		return element;
	}

	public static WebElement orderType() {
		element = BusinessFunctions.getWebDriver().findElement(By.id("portlet:mainForm:priceType"));
		return element;
	}

	public static WebElement orderTerm() {
		element = BusinessFunctions.getWebDriver().findElement(By.id("portlet:mainForm:orderTerm"));
		return element;
	}

	public static WebElement message() {
		element = BusinessFunctions.getWebDriver().findElement(By.className("iceMsgsInfo"));
		return element;
	}

	public static WebElement quote() {
		element = BusinessFunctions.getWebDriver().findElement(By.id("portlet:mainForm:quote"));
		return element;
	}

	public static WebElement price() {
		element = BusinessFunctions.getWebDriver().findElement(By.xpath("//span[@id='bid_price']"));
		return element;
	}

	public static WebElement logo() {
		element = BusinessFunctions.getWebDriver().findElement(By.id("logo"));
		return element;
	}
/*

	@FindBy(xpath=".//*[@id='portlet:mainForm:j_id32']")
	WebElement buy; 	 	 	
	
	@FindBy(id="btn_sell\"")
	WebElement sell;

	@FindBy(id="portlet:mainForm:market")
	WebElement market;
	
	@FindBy(id="portlet:mainForm:symbol")
	WebElement stockCode;
	
	@FindBy(id="portlet:mainForm:limitPrice")
	WebElement orderPrice;
	
	@FindBy(id="portlet:mainForm:orderQty")
	WebElement orderQty;
	
	@FindBy(id="portlet:mainForm:button_previewOrder")
	WebElement prevOrder;
	
	@FindBy(xpath="//a[text()='My Setting']")
	WebElement mysettings;

	@FindBy(xpath="//title[text()='InvestTrade iWeb']")
	WebElement Hometitle;
	
	@FindBy(tagName="Orders")
	WebElement OrderTab;
	
	@FindBy(xpath="//a[text()='Trade']")
	WebElement Trade;
	
	@FindBy(id="portlet:mainForm:priceType")
	WebElement ordType;
	
	@FindBy(id="portlet:mainForm:orderTerm")
	WebElement ordTerm;
	
	@FindBy(className="iceMsgsInfo")
	WebElement message;
	
	@FindBy(id="portlet:mainForm:quote")
	WebElement quote;
	
	@FindBy(xpath="//span[@id='bid_price']")
	WebElement price;
*/



	/*
	public HomePage(WebDriver driver)
	{
		this.driver = driver;

        //This initElements method will create all WebElements

        PageFactory.initElements(driver, this);
        ConfigFileReader configFileReader = new ConfigFileReader();
        wait = new WebDriverWait(driver,30);
	}
	public String getHometitle() {

		return Hometitle.getText();
	}

	public void getMySettings() {
		mysettings.getText();
	}

	public void clickbuy() {

		buy.click();
	}

	public void clicksell() {
		sell.click();

	}

	public void selectmarket(String markettype) {
		market.sendKeys(markettype);
	}

	public void selectstock(String stock) {

		stockCode.sendKeys(stock);

	}
	public void selectorderprice(String price) {
		orderPrice.clear();
		orderPrice.sendKeys(price);

	}

	public String getorderprice() {
		return orderPrice.getAttribute("value");

	}
	public void Clickorderprice() {
		orderPrice.click();

	}

	public void selectorderQty(String qty) {
		orderQty.sendKeys(qty);

	}

	public void selectordType(String type) {
		ordType.sendKeys(type);
	}

	public void selectordTerm(String term) {
		ordTerm.sendKeys(term);
	}

	public void preview() {

		prevOrder.click();
	}

	public void tradeClick() {

		Trade.click();
	}

	public void ordersClick() {

		OrderTab.click();
	}

	public String getmessage(String mess) {

		return message.getText();
	}
	public void browserquit() {

		driver.quit();
	}

	public void getquote() {
		quote.click();
	}

	public void ClickBidPrice() {

		price.click();

	}
	*/



	// username

}