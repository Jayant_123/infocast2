package PageObjectModel;

import cucumber.api.Scenario;
import org.apache.commons.io.FileUtils;
import org.junit.Assert;
import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Actions;
import stepDefinitions.Hooks;

import java.io.File;
import java.io.IOException;
import java.time.Duration;
import java.util.List;

public class BusinessFunctions extends Hooks {

	public static Actions act = new Actions(getWebDriver());
		/*
        '================================================================================================================
        ' FUNCTION NAME	: getWebDriver
        ' PURPOSE		: This function gets the current web driver instance.
        ' PARAMETERS	: in string: url - url you want to open.
                        in string: WE - Web Element identifer (This is be declare in the POM page with proper locator).
          Author		: Jayant S
          Reviewer		: Venkata Kumar B
        '=================================================================================================================
         */
	public static WebDriver getWebDriver() {
		return driver;
	}

		/*
		'================================================================================================================
		' FUNCTION NAME	: OpenURL
		' PURPOSE		: This function Set the value for an Web Edit box.
		' PARAMETERS	: in string: url - url you want to open.
						in string: WE - Web Element identifer (This is be declare in the POM page with proper locator).
		  Author		: Jayant S
		  Reviewer		: Venkata Kumar B
		'=================================================================================================================
		 */
	public static void OpenURL(String url){
		System.out.println(url);
		getWebDriver().get(url);
		getWebDriver().manage().window().maximize();
		//return driver;
		
		}
    public String gettestid()
    {
        return testid;
    }


    /*
		'================================================================================================================
		' FUNCTION NAME	: Click
		' PURPOSE		: This function performs click action on a locator.
		' PARAMETERS	: in string: WE - Web Element identifer.
		  Author		: Jayant S
		  Reviewer		: Venkata Kumar B
		'=================================================================================================================

		 */
	//CLICK ACTION
	public static void Click(WebElement WE) {
		//wait.until(ExpectedConditions.visibilityOf(WE)).click();
		WE.click();


	}

	 	/*
		'================================================================================================================
		' FUNCTION NAME	: Click
		' PURPOSE		: This is used to check, is the check-box is currently checked or unchecked to checked Radio buttons are selected or not.
		' PARAMETERS	: in string: WE - Web Element identifer.
		  Author		: Jayant S
		  Reviewer		: Venkata Kumar B
		'=================================================================================================================

		 */
	public static void isSelected(WebElement WE) {
		//wait.until(ExpectedConditions.visibilityOf(WE)).click();
		WE.isSelected();


	}



	/*
		'================================================================================================================
		' FUNCTION NAME	: Sendkeys
		' PURPOSE		: This function helps to send keys(values) to a locator.
		' PARAMETERS	: WE - WebElement identifer .
						 value - String value.
		  Author		: Jayant S
		  Reviewer		: Venkata Kumar B
		'=================================================================================================================

		 */
	//SENDKEYS ACTION
		public static void Sendkeys(WebElement WE,String value) {

			WE.sendKeys(new CharSequence[]{value});

		}

		/*
		'================================================================================================================
		' FUNCTION NAME	: getTitle
		' PURPOSE		: This function gets the title of the current page.
		' PARAMETERS	: nil
		  Author		: Jayant S
		  Reviewer		: Venkata Kumar B
		'=================================================================================================================

		 */
	//GET TITLE
		public static String getTitle(){

		return getWebDriver().getTitle();
		}

		/*
		'================================================================================================================
		' FUNCTION NAME	: Assert_String_Case
		' PURPOSE		: This function helps in validation.
		' PARAMETERS	: string : expected result
						  string : actual result
		  Author		: Jayant S
		  Reviewer		: Venkata Kumar B
		'=================================================================================================================

		*/

		public static void Assert_String_Case(String expected,String actual)
		{
			Assert.assertEquals(expected, actual);
		}
		/*
		'================================================================================================================
		' FUNCTION NAME	: Assert_Boolean_Case
		' PURPOSE		: This function helps in validation.
		' PARAMETERS	: boolean : expected result
						  boolean : actual result
		  Author		: Jayant S
		  Reviewer		: Venkata Kumar B
		'=================================================================================================================
	    */
		public static void Assert_Boolean_Case(Boolean expected,Boolean actual)
		{
			Assert.assertEquals(expected, actual);

		}

		/*
		'================================================================================================================
		' FUNCTION NAME	: Assert_Byte_Array_Case
		' PURPOSE		: Asserts that two byte arrays are equal.
		' PARAMETERS	: byte[] : expected result
						  byte[] : actual result
		  Author		: Jayant S
		  Reviewer		: Venkata Kumar B
		'=================================================================================================================
		*/
			public static void Assert_Byte_Array_Case(byte[] expecteds, byte[] actuals)
			{
				Assert.assertArrayEquals(expecteds, actuals);

			}

		/*
		'================================================================================================================
		' FUNCTION NAME	: Assert_Char_Array_Case
		' PURPOSE		: This function helps in validation.
		' PARAMETERS	: char[] : expected result
					  	  char[]  : actual result
		Author			: Jayant S
		Reviewer		: Venkata Kumar B
		'=================================================================================================================
		*/
			public static void Assert_Char_Array_Case(char[] expecteds, char[] actuals)
			{
				Assert.assertArrayEquals(expecteds, actuals);
			}

		/*
		'================================================================================================================
		' FUNCTION NAME	: Assert_Int_Array_Case
		' PURPOSE		:  Asserts that two int arrays are equal.
		' PARAMETERS	: int[] : expected result
					  	  int[] : actual result
		Author		    : Jayant S
		Reviewer		: Venkata Kumar B
		'=================================================================================================================
		*/
			public static void Assert_Int_Array_Case(int[] expecteds, int[] actuals)
			{
				Assert.assertArrayEquals(expecteds,actuals);

			}

		/*
		'================================================================================================================
		' FUNCTION NAME	: Assert_Long_Array_Case
		' PURPOSE		:  Asserts that two long arrays are equal.
		' PARAMETERS	: long[] : expected result
							long[] : actual result
		Author		    : Jayant S
		Reviewer		: Venkata Kumar B
		'=================================================================================================================
		*/
			public static void Assert_Long_Array_Case(long[] expcteds, long[] actuals)
			{
				Assert.assertArrayEquals(expcteds,actuals);

			}

		/*
		'================================================================================================================
		' FUNCTION NAME	: Assert_Short_Array_Case
		' PURPOSE		: Asserts that two short arrays are equal.
		' PARAMETERS	: short[] : expected result
						  short[] : actual result
			Author		: Jayant S
			Reviewer	: Venkata Kumar B
		'=================================================================================================================
		*/
			public static void Assert_Short_Array_Case(short[] expcteds, short[] actuals)
			{
				Assert.assertArrayEquals(expcteds, actuals);

			}

		/*
		'================================================================================================================
		' FUNCTION NAME	: Assert_Object_Array_Case
		' PURPOSE		: Asserts that two object arrays are equal.
		' PARAMETERS	: Object[] : expected result
						  Object[] : actual result
		  Author		: Jayant S
		  Reviewer		: Venkata Kumar B
		'=================================================================================================================

		*/

		public static void Assert_Object_Array_Case(Object[] expecteds, Object[] actuals)
		{
			Assert.assertArrayEquals(expecteds, actuals);

		}

				/*
		'================================================================================================================
		' FUNCTION NAME	: Assert_Byte_Array_Message_Case
		' PURPOSE		: Asserts that two byte arrays are equal with a message.
		' PARAMETERS	: byte[] : expected result
						  byte[] : actual result
		  Author		: Jayant S
		  Reviewer		: Venkata Kumar B
		'=================================================================================================================

		*/

		public static void Assert_Byte_Array_Message_Case(String message, byte[] expecteds, byte[] actuals)
		{
			Assert.assertArrayEquals(message, expecteds, actuals);

		}



	/*
		'================================================================================================================
		' FUNCTION NAME	: Assert_Char_Array_Message_Case
		' PURPOSE		:  Asserts that two char arrays are equal with a message.
		' PARAMETERS	: char[] : expected result
						  char[] : actual result
		  Author		: Jayant S
		  Reviewer		: Venkata Kumar B
		'=================================================================================================================

		*/

		public static void Assert_Char_Array_Message_Case(String message, char[] expecteds, char[] actuals)
		{
			Assert.assertArrayEquals(message, expecteds, actuals);
		}


	/*
		'================================================================================================================
		' FUNCTION NAME	: Assert_Int_Array_Message_Case
		' PURPOSE		:  Asserts that two int arrays are equal with a message.
		' PARAMETERS	: int[] : expected result
						  int[] : actual result
		  Author		: Jayant S
		  Reviewer		: Venkata Kumar B
		'=================================================================================================================

		*/

		public static void Assert_Int_Array_Message_Case(String message, int[] expecteds, int[] actuals)
		{
			Assert.assertArrayEquals(message, expecteds, actuals);
		}

	/*
		'================================================================================================================
		' FUNCTION NAME	: Assert_Byte_Array_Message_Case
		' PURPOSE		:  Asserts that two long arrays are equal with a message.
		' PARAMETERS	: long[] : expected result
						  long[] : actual result
		  Author		: Jayant S
		  Reviewer		: Venkata Kumar B
		'=================================================================================================================

		*/

		public static void Assert_Byte_Array_Message_Case(String message, long[] expecteds, long[] actuals)
		{
			Assert.assertArrayEquals(message, expecteds, actuals);
		}



		/*
		'================================================================================================================
		' FUNCTION NAME	: Assert_String_Array_Message_Case
		' PURPOSE		:  Asserts that two Object arrays are equal with a message.
		' PARAMETERS	: Object[] : expected result
						  Object[] : actual result
		  Author		: Jayant S
		  Reviewer		: Venkata Kumar B
		'=================================================================================================================

		*/

		public static void Assert_String_Array_Message_Case(String message, Object[] expecteds, Object[] actuals)
		{
			Assert.assertArrayEquals(message, expecteds, actuals);
		}


	/*
		'================================================================================================================
		' FUNCTION NAME	: Assert_Short_Array_Message_Case
		' PURPOSE		:  Asserts that two short arrays are equal with a message.
		' PARAMETERS	: short[] : expected result
						  short[] : actual result
		  Author		: Jayant S
		  Reviewer		: Venkata Kumar B
		'=================================================================================================================

		*/

		public static void Assert_Short_Array_Message_Case(String message, short[] expecteds, short[] actuals)
		{
			Assert.assertArrayEquals(message, expecteds, actuals);
		}




	/*
		'================================================================================================================
		' FUNCTION NAME	: AssertCondition
		' PURPOSE		: This function helps in validation.
		' PARAMETERS	: boolean : condition

		  Author		: Jayant S
		  Reviewer		: Venkata Kumar B
		'=================================================================================================================
	    */
		public static void AssertTrue_Boolean(Boolean condition)
		{
			Assert.assertTrue(condition);

		}


		/*
		'================================================================================================================
		' FUNCTION NAME	: AssertCondition
		' PURPOSE		: This function helps in validation.
		' PARAMETERS	: boolean : condition

		  Author		: Jayant S
		  Reviewer		: Venkata Kumar B
		'=================================================================================================================
		*/
		public static void Assert_Object_Case(Object expected,Object actual)
		{
			Assert.assertEquals(expected, actual);

		}

		/*
		'================================================================================================================
		' FUNCTION NAME	: Assert_False_Boolean
		' PURPOSE		:  Asserts that a condition is false.
		' PARAMETERS	: boolean : condition

		  Author		: Jayant S
		  Reviewer		: Venkata Kumar B
		'=================================================================================================================
		*/
		public static void Assert_False_Boolean(Boolean condition)
		{
			Assert.assertFalse(condition);

		}



		/*
		'================================================================================================================
		' FUNCTION NAME	: selectFrame
		' PURPOSE		: This function jumps to the frame you want to move
		' PARAMETERS	: String : nameorId - frame name or id
		  Author		: Jayant S
		  Reviewer		: Venkata Kumar B
		'=================================================================================================================
	    */

		public static void selectFrame(String nameorId)
		{
			getWebDriver().switchTo().frame(nameorId);
		}

		/*
		'================================================================================================================
		' FUNCTION NAME	: selectFrame
		' PURPOSE		: This function jumps to the frame you want to move
		' PARAMETERS	: int : no - frame no
		  Author		: Jayant S
		  Reviewer		: Venkata Kumar B
		'=================================================================================================================
	    */

	public static void selectFrame(int no)
	{
		getWebDriver().switchTo().frame(no);
	}

		/*
		'================================================================================================================
		' FUNCTION NAME	: selectFrame
		' PURPOSE		: his function jumps to the frame you want to move.
		' PARAMETERS	: WebElement : WE
		  Author		: Jayant S
		  Reviewer		: Venkata Kumar B
		'=================================================================================================================
		*/

	public static void selectFrame(WebElement WE) {
		getWebDriver().switchTo().frame(WE);
	}

		/*
		'================================================================================================================
		' FUNCTION NAME	: getText
		' PURPOSE		: This function gets the text from the WebElement.
		' PARAMETERS	: WE - Web Element identifer
		  Author		: Jayant S
		  Reviewer		: Venkata Kumar B
		'=================================================================================================================
	    */

		public static String getText(WebElement WE)
		{
			return WE.getText();

		}

		/*
		'================================================================================================================
		' FUNCTION NAME	: parentFrame
		' PURPOSE		: This function switches from its original location to the parent frame
		' PARAMETERS	: nil
		  Author		: Jayant S
		  Reviewer		: Venkata Kumar B
		'=================================================================================================================
		*/

		public static void parentFrame()
		{
			getWebDriver().switchTo().parentFrame();
		}


		/*
		'================================================================================================================
		' FUNCTION NAME	: quitBrowser
		' PURPOSE		: This function helps to end the browser session.
		' PARAMETERS	: nil
		  Author		: Jayant S
		  Reviewer		: Venkata Kumar B
		'=================================================================================================================
	    */
	    public static void quitBrowser(){

			getWebDriver().quit();
	    }


		/*
		'================================================================================================================
		' FUNCTION NAME	: clearField
		' PURPOSE		: This function helps to clear data in a text field.
		' PARAMETERS	: nil
		  Author		: Jayant S
		  Reviewer		: Venkata Kumar B
		'=================================================================================================================
		 */

	    public static void clearField(WebElement WE){

	    	WE.clear();
		}

		/*
		'================================================================================================================
		' FUNCTION NAME	: getCurrentUrl
		' PURPOSE		: This function helps to get the current url for the application.
		' PARAMETERS	: nil
		  Author		: Jayant S
		  Reviewer		: Venkata Kumar B
		'=================================================================================================================
		*/

		public static void getCurrentUrl(){
			getWebDriver().getCurrentUrl();
		}

		/*
		'================================================================================================================
		' FUNCTION NAME	: getElementLocation
		' PURPOSE		: This function helps to get the location for the element.
		' PARAMETERS	:  WE - Web Element identifer
		  Author		: Jayant S
		  Reviewer		: Venkata Kumar B
		'=================================================================================================================
	    */

	public static void getElementLocation(WebElement WE){
		WE.getLocation();
	}

		/*
		'================================================================================================================
		' FUNCTION NAME	: getTagName
		' PURPOSE		: This function helps to get the tag name for the element.
		' PARAMETERS	:  WE - Web Element identifer
		  Author		: Jayant S
		  Reviewer		: Venkata Kumar B
		'=================================================================================================================
		*/

	public static void getCurrentTagName(WebElement WE){
		WE.getTagName();
	}

		/*
		'================================================================================================================
		' FUNCTION NAME	: getLocation
		' PURPOSE		: This function helps to get the current location on the page.
		' PARAMETERS	:  WE - Web Element identifer
		  Author		: Jayant S
		  Reviewer		: Venkata Kumar B
		'=================================================================================================================
		*/

	public static void getLocation(WebElement WE) {
		WE.getLocation();
	}


		/*
		'================================================================================================================
		' FUNCTION NAME	: getSize
		' PURPOSE		: This function helps to get the size of the web element.
		' PARAMETERS	:  WE - Web Element identifer
		  Author		: Jayant S
		  Reviewer		: Venkata Kumar B
		'=================================================================================================================
		*/

	public static void getSizeOfElement(WebElement WE) {
		WE.getSize();
	}

		/*
		'================================================================================================================
		' FUNCTION NAME	: isDisplayed
		' PURPOSE		: This function helps to check whether the element is displayed or not.
		' PARAMETERS	:  WE - Web Element identifier
		  Author		: Jayant S
		  Reviewer		: Venkata Kumar B
		'=================================================================================================================
		*/

	public static void isDisplayed(WebElement WE) {
		WE.isDisplayed();
	}

		/*
		'================================================================================================================
		' FUNCTION NAME	: isDisplayed
		' PURPOSE		: This function helps to check whether the element is displayed or not.
		' PARAMETERS	:  WE - Web Element identifier
						   attribute - String
		  Author		: Jayant S
		  Reviewer		: Venkata Kumar B
		'=================================================================================================================
		*/

	public static void isDisplayed(WebElement WE,String attribute) {
		WE.getAttribute(attribute);
	}

		/*
		'================================================================================================================
		' FUNCTION NAME	: isEnabled
		' PURPOSE		: This is a Boolean condition. It is used to either an element is enable or not.
						  If an element is enable it gives true and an element is disable it gives false.
		' PARAMETERS	:  WE - Web Element identifier
		  Author		: Jayant S
		  Reviewer		: Venkata Kumar B
		'=================================================================================================================
		*/

	public static void isEnabled(WebElement WE) {
		WE.isEnabled();
	}


		/*
		'================================================================================================================
		' FUNCTION NAME	: navigateToURL
		' PURPOSE		: This function helps to navigate to a url.
		' PARAMETERS	:  Stirng - Url
		  Author		: Jayant S
		  Reviewer		: Venkata Kumar B
		'=================================================================================================================
		*/

	public static void navigateToURL(String url) {
		getWebDriver().navigate().to(url);
	}

		/*
		'================================================================================================================
		' FUNCTION NAME	: getWindowHandle
		' PURPOSE		: This function helps to return a string of alphanumeric window handle.
		' PARAMETERS	: nil
		  Author		: Jayant S
		  Reviewer		: Venkata Kumar B
		'=================================================================================================================
		*/

	public static void getWindowHandle() {
		getWebDriver().getWindowHandle();
	}


		/*
		'================================================================================================================
		' FUNCTION NAME	: getWindowHandles
		' PURPOSE		: This function helps to return a set of window handle.
		' PARAMETERS	: nil
		  Author		: Jayant S
		  Reviewer		: Venkata Kumar B
		'=================================================================================================================
		*/

	public static void getWindowHandles() {
		getWebDriver().getWindowHandles();
	}

	     /*
		'================================================================================================================
		' FUNCTION NAME	: getPageSource
		' PURPOSE		: This function helps to to validate or verify any text on the page.
		' PARAMETERS	: nil
		  Author		: Jayant S
		  Reviewer		: Venkata Kumar B
		'=================================================================================================================
		*/

	public static void getPageSource() {
		getWebDriver().getPageSource();
	}




		/*
		'================================================================================================================
		' FUNCTION NAME	: maxamize
		' PURPOSE		: This function helps to maximize the window.
		' PARAMETERS	: nil
		  Author		: Jayant S
		  Reviewer		: Venkata Kumar B
		'=================================================================================================================
		*/

	public static void maxamize(){
		getWebDriver().manage().window().maximize();
	}

		/*
		'================================================================================================================
		' FUNCTION NAME	: captureScreenshot
		' PURPOSE		: This function helps to capture Screenshot at that instance.
		' PARAMETERS	: nil
		  Author		: Jayant S
		  Reviewer		: Venkata Kumar B
		'=================================================================================================================
		*/

	public static void captureScreenshot() throws IOException
	{
		File screenshotFile = ((TakesScreenshot)getWebDriver()).getScreenshotAs(OutputType.FILE);
		FileUtils.copyFile(screenshotFile, new File(System.getProperty("user.dir")+"\\target\\FailedTestCase.png"));
	}

		/*

		'================================================================================================================
		' FUNCTION NAME	: actiondragAndDrop
		' PURPOSE		: This function helps to drag an element from its location to another location.
		' PARAMETERS	: source : WebElement
						  destination : WebElement
		  Author		: Jayant S
		  Reviewer		: Venkata Kumar B
		'=================================================================================================================
		*/

	public static void actiondragAndDrop(WebElement source,WebElement destination){
		act.dragAndDrop(source,destination);
	}

		/*
		'================================================================================================================
		' FUNCTION NAME	: actiondoubleClick
		' PURPOSE		: This function helps to double click at a locator.
		' PARAMETERS	: source : WebElement
		  Author		: Jayant S
		  Reviewer		: Venkata Kumar B
		'=================================================================================================================
		*/

	public static void actiondoubleClick(WebElement WE){
		act.doubleClick(WE);
	}

		/*
		'================================================================================================================
		' FUNCTION NAME	: actiondoubleClick
		' PURPOSE		: This function helps to double click at any location.
		' PARAMETERS	: nil
		  Author		: Jayant S
		  Reviewer		: Venkata Kumar B
		'=================================================================================================================
		*/

	public static void actiondoubleClick(){
		act.doubleClick();
	}


		/*
		'================================================================================================================
		' FUNCTION NAME	: actionClick
		' PURPOSE		: This function helps to click at any location.
		' PARAMETERS	: nil
		  Author		: Jayant S
		  Reviewer		: Venkata Kumar B
		'=================================================================================================================
		*/

	public static void actionClick(){
		act.click();
	}

		/*
		'================================================================================================================
		' FUNCTION NAME	: actionClick
		' PURPOSE		: This function helps to click at a specific locator using action class.
		' PARAMETERS	: source : WebElement
		  Author		: Jayant S
		  Reviewer		: Venkata Kumar B
		'=================================================================================================================
		*/

	public static void actionClick(WebElement WE){
		act.click(WE);
	}

		/*
		'================================================================================================================
		' FUNCTION NAME	: actionclickAndHold
		' PURPOSE		: This function helps to click and hold at a specific locator using action class.
		' PARAMETERS	: source : WebElement
		  Author		: Jayant S
		  Reviewer		: Venkata Kumar B
		'=================================================================================================================
		*/

	public static void actionclickAndHold(WebElement WE){
		act.clickAndHold(WE);
	}

		/*
		'================================================================================================================
		' FUNCTION NAME	: actionclickAndHold
		' PURPOSE		: This function helps to click and hold at a specific locator using action class.
		' PARAMETERS	: source : WebElement
		  Author		: Jayant S
		  Reviewer		: Venkata Kumar B
		'=================================================================================================================
		*/

	public static void actionclickAnHold(WebElement WE){
		act.clickAndHold(WE);
	}

		/*
		'================================================================================================================
		' FUNCTION NAME	: actionPause
		' PURPOSE		: This function helps to pause for a specific duration of time.
		' PARAMETERS	: Duration : duration
		  Author		: Jayant S
		  Reviewer		: Venkata Kumar B
		'=================================================================================================================
		*/

	public static void actionPause(Duration duration){
		act.pause(duration);
	}


		/*
		'================================================================================================================
		' FUNCTION NAME	: actionContextClick
		' PURPOSE		:	Performs a context-click at middle of the given element.
		' PARAMETERS	:  source : WebElement
		  Author		: Jayant S
		  Reviewer		: Venkata Kumar B
		'=================================================================================================================
		*/

	public static void actionRightClick(WebElement WE){
		act.contextClick(WE);
	}

		/*
		'================================================================================================================
		' FUNCTION NAME	: actionKeyUp
		' PURPOSE		: This function helps to push a key up
		' PARAMETERS	: String : key
		  Author		: Jayant S
		  Reviewer		: Venkata Kumar B
		'=================================================================================================================
		*/

	public static void actionKeyUp(String key){
		act.keyUp(key);
	}

		/*
		'================================================================================================================
		' FUNCTION NAME	: actionKeyUp
		' PURPOSE		: Performs a modifier key release.
		' PARAMETERS	: source : WebElement
						  String : key
		  Author		: Jayant S
		  Reviewer		: Venkata Kumar B
		'=================================================================================================================
		*/

	public static void actionKeyUp(WebElement WE,String key){
		act.keyUp(WE,key);
	}

		/*
		'================================================================================================================
		' FUNCTION NAME	: actionKeyDown
		' PURPOSE		: Performs a modifier key release after focusing on an element.
		' PARAMETERS	: String : key
		  Author		: Jayant S
		  Reviewer		: Venkata Kumar B
		'=================================================================================================================
		*/

	public static void actionKeyDown(String key){
		act.keyDown(key);
	}

		/*
		'================================================================================================================
		' FUNCTION NAME	: actionKeyDown
		' PURPOSE		: Performs a modifier key press.
		' PARAMETERS	: source : WebElement
						  String : key
		  Author		: Jayant S
		  Reviewer		: Venkata Kumar B
		'=================================================================================================================
		*/

	public static void actionKeyDown(WebElement WE,String key){
		act.keyDown(WE,key);
	}

	/*
		'================================================================================================================
		' FUNCTION NAME	: actionMoveByOffset
		' PURPOSE		: Moves the mouse from its current position (or 0,0) by the given offset.
		' PARAMETERS	: xOffset : int
						  yOffset : int
		  Author		: Jayant S
		  Reviewer		: Venkata Kumar B
		'=================================================================================================================
		*/

	public static void actionMoveByOffset(int xOffset, int yOffset){
		act.moveByOffset(xOffset,yOffset);
	}

	/*
		'================================================================================================================
		' FUNCTION NAME	: actionMoveToElement
		' PURPOSE		: Moves the mouse to the middle of the element.
		' PARAMETERS	: WebElement : WE
		  Author		: Jayant S
		  Reviewer		: Venkata Kumar B
		'=================================================================================================================
		*/

	public static void actionMoveToElement(WebElement WE){
		act.moveToElement(WE);
	}

		/*
		'================================================================================================================
		' FUNCTION NAME	: actionMoveToElement
		' PURPOSE		: Moves the mouse to the middle of the element.
		' PARAMETERS	: WebElement : WE
						  xOffset : int
						  yOffset : int
		  Author		: Jayant S
		  Reviewer		: Venkata Kumar B
		'=================================================================================================================
		*/

	public static void actionMoveToElement(WebElement WE, int xOffset, int yOffset){
		act.moveToElement(WE,xOffset,yOffset);
	}

		/*
		'================================================================================================================
		' FUNCTION NAME	: actionPerform
		' PURPOSE		: A convenience method for performing the actions without calling build() first.
		' PARAMETERS	: nil
		  Author		: Jayant S
		  Reviewer		: Venkata Kumar B
		'=================================================================================================================
		*/

	public static void actionPerform(){
		act.perform();
	}

		/*
		'================================================================================================================
		' FUNCTION NAME	: actionRelease
		' PURPOSE		: Releases the depressed left mouse button at the current mouse location.
		' PARAMETERS	: nil
		  Author		: Jayant S
		  Reviewer		: Venkata Kumar B
		'=================================================================================================================
		*/

	public static void actionRelease(){
		act.release();
	}

			/*
		'================================================================================================================
		' FUNCTION NAME	: actionRelease
		' PURPOSE		: Releases the depressed left mouse button, in the middle of the given element.
		' PARAMETERS	: WebElement : WE
		  Author		: Jayant S
		  Reviewer		: Venkata Kumar B
		'=================================================================================================================
		*/

	public static void actionRelease(WebElement WE){
		act.release(WE);
	}


			/*
		'================================================================================================================
		' FUNCTION NAME	: actionSendKeys
		' PURPOSE		: Sends keys to the active element.
		' PARAMETERS	: String : key
		  Author		: Jayant S
		  Reviewer		: Venkata Kumar B
		'=================================================================================================================
		*/

	public static void actionSendKeys(String key){
		act.sendKeys(key);
	}

				/*
		'================================================================================================================
		' FUNCTION NAME	: actionSendKeys
		' PURPOSE		: Sends keys to the the specific element.
		' PARAMETERS	: String : key
		  Author		: Jayant S
		  Reviewer		: Venkata Kumar B
		'=================================================================================================================
		*/

	public static void actionSendKeys(WebElement WE,String key){
		act.sendKeys(WE,key);
	}

		/*
		'================================================================================================================
		' FUNCTION NAME	: actionBuild
		' PURPOSE		: Generates a composite action containing all actions so far, ready to be performed (and resets the internal builder state, so subsequent calls to build() will contain fresh sequences).
		' PARAMETERS	: nil
		  Author		: Jayant S
		  Reviewer		: Venkata Kumar B
		'=================================================================================================================
		*/

	public static void actionBuild(){
		act.build();
	}

	public static void calenderPick(WebElement selectCalender, WebElement pickMonth,WebElement pickdate) {


	}

}

