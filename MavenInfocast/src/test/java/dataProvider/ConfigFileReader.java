package dataProvider;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Properties;



public class ConfigFileReader {
	
	private Properties properties;
	private final String propertyFilePath="E:\\MavenInfocast\\configs\\Configuation.properties";

	

	public ConfigFileReader(){
		BufferedReader reader;
		try {
			reader = new BufferedReader(new FileReader(propertyFilePath));
			properties = new Properties();
			try {
				properties.load(reader);
				reader.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			throw new RuntimeException("Configuration.properties not found at " + propertyFilePath);
		}		
	}
	
	private String getCurrentDirectory() {
		// TODO Auto-generated method stub
		return null;
	}

	public String getDriverPath() {
		String driverPath=properties.getProperty("driverPath");
		if(driverPath!=null) return driverPath;
		else throw new RuntimeException("driverPath not specified in the Configuration.properties file.");
	}
	public String getImplicitlyWait() {
		String implcitlyWait=properties.getProperty("impilcitlyWait");
		if(implcitlyWait != null) return implcitlyWait;
		else throw new RuntimeException("implicitlyWait not specified in the Configuration.properties file.");
		
	}
	public String getApplicationUrl() {
		String url=properties.getProperty("url");
		if(url != null) return url;
		else throw new RuntimeException("url not specified in the Configuration.properties file.");
	}
	public String getusername() {
		String username=properties.getProperty("username");
		if(username !=null) return username;
		else throw new RuntimeException("username not specified in the Configuration.properties file.");
	}
	public String getpassword() {
		String password=properties.getProperty("password");
		if(password !=null) return password;
		else throw new RuntimeException("password not specified in the Configuration.properties file.");
	}
	
	public String gettempBuild() {
		String tempBuild=properties.getProperty("tempbuild");
		if(tempBuild !=null) return tempBuild;
		else throw new RuntimeException("tempbuild not specified in the Configuration.properties file.");
	}


	public String getReportConfigPath(){
		String reportConfigPath = properties.getProperty("reportConfigPath");
		if(reportConfigPath!= null) return reportConfigPath;
		else throw new RuntimeException("Report Config Path not specified in the Configuration.properties file for the Key:reportConfigPath");		
}
	
	
}
