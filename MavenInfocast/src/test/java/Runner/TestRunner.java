package Runner;

import org.junit.runner.RunWith;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;

@RunWith(Cucumber.class)
@CucumberOptions(
		
		features = "src\\main\\resources\\Features",
		glue={"stepDefinitions"},
		tags = {"@login"},
		//plugin = {"html:target/cucumber-html-report"},
		plugin = { "pretty",
				   "html:target/site/cucumber-pretty",
			    "json:target/cucumber.json" },
	//	format= {"html:target/cucumber-reports"}, //to generate different types of reporting
		monochrome = true, 
		strict = true,
		//plugin = { "com.cucumber.listener.ExtentCucumberFormatter:target/cucumber-reports/report.html"},
		dryRun = true
				)

public class TestRunner {
	/*@AfterClass
	public static void writeExtentReport() {
		Reporter.loadXMLConfig(FileReaderManager.getInstance().getConfigReader().getReportConfigPath());
		//Reporter.loadXMLConfig("configs/extent-config.xml");
	}*/

	
	
}
