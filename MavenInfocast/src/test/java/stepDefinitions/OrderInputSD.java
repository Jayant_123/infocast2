package stepDefinitions;

import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.io.File;

import PageObjectModel.BusinessFunctions;
import PageObjectModel.ChangePassPage;
import PageObjectModel.LoginPF;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;



import PageObjectModel.HomePage;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import junit.framework.Assert;
import testlink.api.java.client.TestLinkAPIException;
import testlink.api.java.client.TestLinkAPIResults;
import utility.ConfigFileReader;
import utility.TestlinkIntegration;



public class OrderInputSD {

	
	WebDriver driver;
	
	ConfigFileReader configFileReader;
	
	public static String testno;
	static String web_app = System.getProperty("web_application");
	static String App_Username = System.getProperty("app_username");
	static String App_Password = System.getProperty("app_password");
	public static String browser = System.getProperty("browser_name");

	
	@Given("^user logged with \\\"([^\\\"]*)\\\" and \\\"([^\\\"]*)\\\" and submit$")
	public void user_logged_in_to_the_application(String username, String password) throws Exception {
	/*
		Process process= Runtime.getRuntime().exec("taskkill /F /IM chromedriver.exe /T");
		process.destroy();
		

		
		configFileReader= new ConfigFileReader();
		System.setProperty("webdriver.chrome.driver",System.getProperty("user.dir")+"\\lib\\chromeP\\chromedriver.exe");
		driver=new ChromeDriver();
		driver.manage().window().maximize();
		
		driver.get("https://iweb-qa2.infocast.hk/cas/login?locale=en_US");
		
		*/

		BusinessFunctions.OpenURL(web_app);
		Thread.sleep(2000);
		BusinessFunctions.Sendkeys(LoginPF.username(), App_Username);
		BusinessFunctions.Sendkeys(LoginPF.password(), App_Password);
		BusinessFunctions.Click(LoginPF.submit());
		Thread.sleep(2000);
		BusinessFunctions.Click(LoginPF.agree());
		/*LoginPF login=new LoginPF(driver);
		login.loginProcedure(configFileReader.getusername(),configFileReader.getpassword());*/
		
	}

	@When("^user click on buy icon$")
	public void user_click_on_buy_icon() throws Exception {
			Thread.sleep(6000);
			/*
			driver.switchTo().frame("orderInputIframe");
	    
		
	        HomePage home = new HomePage(driver);
	        home.clickbuy();
*/
	        BusinessFunctions.selectFrame("orderInputIframe");
	        BusinessFunctions.Click(HomePage.buy());

	}

	@Then("^user select type of market as \"([^\"]*)\"$")
	public void user_select_type_of_market_as(String arg1) {
	/*	HomePage home = new HomePage(driver);
		home.selectmarket(arg1);
		*/
		BusinessFunctions.Click(HomePage.market());
		
	}

	@Then("^user enter stock code as \"([^\"]*)\"$")
	public void user_enter_stock_code_as(String arg1) {
		/*HomePage home = new HomePage(driver);
		home.selectstock(arg1);*/

		BusinessFunctions.Click(HomePage.stockCode());
		
	}

	@Then("^order price as \"([^\"]*)\"$")
	public void order_price_as(String arg1){
		/*HomePage home = new HomePage(driver);
		home.selectorderprice(arg1);*/

		BusinessFunctions.Sendkeys(HomePage.orderPrice(),arg1);

		
		
	}

	@Then("^order quantity as \"([^\"]*)\"$")
	public void order_quantity_as(String arg1) {
		/*HomePage home = new HomePage(driver);
		home.selectorderQty(arg1);*/

		BusinessFunctions.Sendkeys(HomePage.orderQuantity(),arg1);

		
		
	}
	@Then("^order type as \"([^\"]*)\"$")
	public void order_type_as(String arg1) {
		/*HomePage home = new HomePage(driver);
		home.selectordType(arg1);
		*/
		BusinessFunctions.Sendkeys(HomePage.orderType(),arg1);
	}

	@Then("^order term as \"([^\"]*)\"$")
	public void order_term_as(String arg1) {
	/*	HomePage home = new HomePage(driver);
		home.selectordTerm(arg1);
		*/

	BusinessFunctions.Sendkeys(HomePage.orderTerm(),arg1);
		
	}

	@Then("^Preview order$")
	public void preview_order() {
		
	/*	HomePage home = new HomePage(driver);
		home.preview();*/

	BusinessFunctions.Click(HomePage.previewOrder());
		
		
	}
	
	@Then("^message given is \"([^\"]*)\" with  testcase\"([^\"]*)\"$")
	public void message_given_is_with_testcase(String arg1, String arg2) throws TestLinkAPIException,Exception{
	    Thread.sleep(2000);
	   /* HomePage home = new HomePage(driver);
		String se = home.getmessage(arg1);*/
		String se = BusinessFunctions.getText(HomePage.message());
		testno = arg2;
		BusinessFunctions.Assert_String_Case(arg1,se);

	}
	
	public static String testName()
	{
		return testno;
	}
	
	
	@Then("^close the current browser$")
	public void close_the_current_browser() throws TestLinkAPIException, InterruptedException {
		/*HomePage home = new HomePage(driver);
		home.browserquit();
		*/
/*		Thread.sleep(2000);
		BusinessFunctions.parentFrame();
		Thread.sleep(2000);
		BusinessFunctions.Click(HomePage.logo());*/
		Thread.sleep(2000);
		BusinessFunctions.parentFrame();
		Thread.sleep(2000);
		BusinessFunctions.Click(ChangePassPage.signOut());
		
	}
/*

	@When("^enter order price with nominal price$")
	public void enter_order_price_with_nominal_price() throws Exception{
		HomePage home = new HomePage(driver);
		Thread.sleep(4000);
		home.getquote();
		
		
		Thread.sleep(4000);
		driver.switchTo().parentFrame();
		Thread.sleep(2000);
		driver.switchTo().frame("amsSnapshotIframe");
		
		Thread.sleep(2000);
		home.ClickBidPrice();
		Thread.sleep(2000);
		driver.switchTo().parentFrame();
		Thread.sleep(2000);
		driver.switchTo().frame("orderInputIframe");
		
		Thread.sleep(1000);
		home.Clickorderprice();
	
	}

	@When("^user performs \"([^\"]*)\" on order price to price incremented by \"([^\"]*)\"$")
	public void user_performs_on_order_price_to_price_incremented_by(String arg1, String arg2)  throws Exception {

		HomePage home = new HomePage(driver);
		String normalPrice = home.getorderprice();
		home.Clickorderprice();
		
	
		
		Robot rb = new Robot();
		Thread.sleep(1000);
		if (arg1.equals("Up arrow"))
		{
			Thread.sleep(2000);
				rb.keyPress(KeyEvent.VK_UP);
				rb.keyRelease(KeyEvent.VK_UP);
				Thread.sleep(2000);
				String changePrice = home.getorderprice();
				Float up = Float.valueOf(changePrice) - Float.valueOf(normalPrice);
				Assert.assertEquals(arg2,String.format("%.3f",up));
		}
				else
				{
				rb.keyPress(KeyEvent.VK_DOWN);
				rb.keyRelease(KeyEvent.VK_DOWN);
				Thread.sleep(2000);
				String changePrice = home.getorderprice();
				Thread.sleep(1000);
				Float down = Float.valueOf(normalPrice) - Float.valueOf(changePrice);
				Assert.assertEquals(arg2,String.format("%.3f",down));
				}
		}
*/

/*		@After
		public void tearDown(Scenario scenario) throws Throwable {
			
		    try {
		        if (scenario.isFailed()) {
	
		            final byte[] screenshot = ((TakesScreenshot) driver).getScreenshotAs(org.openqa.selenium.OutputType.BYTES);
		            scenario.embed(screenshot, "image/png");
		        }
		    } catch (WebDriverException somePlatformsDontSupportScreenshots) {
		        System.err.println(somePlatformsDontSupportScreenshots.getMessage());
		    } finally {
		       
		    }

	}
*/
	
}
