package stepDefinitions;

import PageObjectModel.ChangePassPage;
import PageObjectModel.LoginPF;
import org.openqa.selenium.WebDriverException;

import PageObjectModel.BusinessFunctions;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import testlink.api.java.client.TestLinkAPIResults;
import utility.TestlinkIntegration;


public class ChangePassword{
	

	
	String newPassword ="";
	public static String testno;
	static String web_app = System.getProperty("web_application");
	static String App_Username = System.getProperty("app_username");
	static String App_Password = System.getProperty("app_password");
	public static String browser = System.getProperty("browser_name");
	

	@Given("^user enters \\\"([^\\\"]*)\\\" and \\\"([^\\\"]*)\\\" and submit..$")
	public void loginProcedure(String username, String password) throws Exception {

			BusinessFunctions.OpenURL(web_app);
			Thread.sleep(2000);
			BusinessFunctions.Sendkeys(LoginPF.username(), App_Username);
			BusinessFunctions.Sendkeys(LoginPF.password(), App_Password);
			BusinessFunctions.Click(LoginPF.submit());
			Thread.sleep(2000);
			BusinessFunctions.Click(LoginPF.agree());

	}

	@Then("^User should be on homepage$")
	public void user_already_on_home() throws Exception {

	    Thread.sleep(5000);

		BusinessFunctions.Assert_String_Case("InvestTrade iWeb",BusinessFunctions.getTitle());

	    }

	@Then("^User clicks on My setting and change password$")
	public void user_clicks_on_My_setting_and_change_password() throws Exception {
		

		 Thread.sleep(4000);
		 

		 Thread.sleep(1000);
		 BusinessFunctions.Click(ChangePassPage.mySettings());

		 BusinessFunctions.Click(ChangePassPage.changePassword());
	}

	@Then("^redirect to change password page$")
	public void redirect_to_change_password_page() throws Exception  {
		Thread.sleep(4000);

		  
		 BusinessFunctions.Assert_String_Case("Change Password",BusinessFunctions.getText(ChangePassPage.changePPage()));
		  Thread.sleep(2000);

	}

	

	
	@When("^user enters Old Password as \"([^\"]*)\"$")
	public void user_enters(String old) throws Exception {
		
		Thread.sleep(3000);
		BusinessFunctions.selectFrame(3);
		BusinessFunctions.Sendkeys(ChangePassPage.oldPassword(),old);

	}

	@When("^user enters New password as \"([^\"]*)\"$")
	public void user_enters_tesr(String neww) throws Exception {
		Thread.sleep(3000);
		
		this.newPassword=neww;

		BusinessFunctions.Sendkeys(ChangePassPage.newPassword(),neww);

		Thread.sleep(1000);

	}

	@When("^user enter Confirm password as \"([^\"]*)\"$")
	public void user_enter_confirm_new_password(String confirm) throws Exception {
		Thread.sleep(1000);

		BusinessFunctions.Sendkeys(ChangePassPage.confirmPassword(),confirm);

	}
	
	
	@Then("^error message given is \"([^\"]*)\" with testid \"([^\"]*)\"$")
	public void error_message_given_is_with_testid(String arg1, String arg2) throws InterruptedException,Exception  {
	    Thread.sleep(2000);
		BusinessFunctions.Assert_String_Case(arg1,BusinessFunctions.getText(ChangePassPage.errorMessage()));

	}
	
	public static String testName()
	{
		return testno;
	}
	
	
	@Then("^a message is shown as \"([^\"]*)\"$")
	public void a_message_is_shown_as(String arg1)throws Exception
	{
	    Thread.sleep(2000);

	  BusinessFunctions.parentFrame();
	  String text = BusinessFunctions.getText(ChangePassPage.errorMessage());
	  boolean a=text.contains("Password changed successfully at");
	  boolean b=arg1.contains("Password changed successfully at");
	  BusinessFunctions.Assert_Boolean_Case(a,b);

	}
	@Then("^user clicks sign out$")
	public void user_clicks_sign_out() throws Exception {
		

		BusinessFunctions.parentFrame();
		Thread.sleep(3000);

	}

	@Then("^user is on login page$")
	public void user_is_on_login_page() throws Throwable {
	//	String title=driver.getTitle();
	//	Assert.assertEquals("Infocast", title);

	}

	@When("^User submit username and new password$")
	public void user_submit_username_and_new_password() throws Throwable {
		

	}
	@Then("^user clicks on save$")
	public void user_clicks_on_save() 
	{
		
		BusinessFunctions.Click(ChangePassPage.passwordSave());

	}
	
	
	@Then("^close the browser$")
	public void close_the_browser() {
	
		BusinessFunctions.parentFrame();
		BusinessFunctions.Click(ChangePassPage.signOut());
	}


	@Given("^Verify Testcase with testid \"([^\"]*)\"$")
	public void verify_Testcase_with_testid(String testcaseID) {
		Hooks.testid = testcaseID;
	}



}
	
