package stepDefinitions;

import java.io.File;
import java.io.IOException;

import PageObjectModel.BusinessFunctions;
import PageObjectModel.ChangePassPage;
import PageObjectModel.LoginPF;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.chrome.ChromeDriver;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import junit.framework.Assert;
import net.rcarz.jiraclient.JiraException;
import testlink.api.java.client.TestLinkAPIException;
import utility.ConfigFileReader;

public class Login {
	
	WebDriver driver;
	ConfigFileReader configFileReader;
	public static String testno;
	static String web_app = System.getProperty("web_application");
	static String App_Username = System.getProperty("app_username");
	static String App_Password = System.getProperty("app_password");
	public static String browser = System.getProperty("browser_name");
	
	@Given("^user is already on Login Page$")
	public void user_is_already_on_Login_Page() throws Exception{

		BusinessFunctions.OpenURL(web_app);
	
	}


	@When("^title of login page is Infocast$")
	public void title_of_login_page_is_Infocast(){

	    BusinessFunctions.Assert_String_Case("Infocast",BusinessFunctions.getTitle());

	}

	@Then("^user is on home page with testid \"([^\"]*)\"$")
	public void user_is_on_home_page_with_testid(String arg1) throws TestLinkAPIException, IOException, JiraException, InterruptedException {
		

		BusinessFunctions.Assert_String_Case("InvestTrade iWeb",BusinessFunctions.getTitle());
		Thread.sleep(4000);
		Hooks.testid = arg1;
	}
	
	@Then("^user enters incorrect username and correct password$")
	public void user_enters_incorrect_username_and_correct_password() {



		BusinessFunctions.Sendkeys(LoginPF.username(),"varun");
		BusinessFunctions.Sendkeys(LoginPF.password(),"test1234");
	}

	@Then("^user shows an invalid message$")
	public void user_shows_an_invalid_message()  {
	    
		boolean text = BusinessFunctions.getText(LoginPF.loginStatus()).contains("Invalid User ID or password");
		BusinessFunctions.AssertTrue_Boolean(text);
	}
	
	@Then("^user enters correct username and incorrect password$")
	public void user_enters_correct_username_and_incorrect_password()  {
		BusinessFunctions.Sendkeys(LoginPF.username(),"qadmuser7");
		BusinessFunctions.Sendkeys(LoginPF.password(),"test");
	}
	@Then("^user clicks on submit button$")
	public void user_clicks_on_submit_button(){
		
		BusinessFunctions.Click(LoginPF.submit());
	    
	}
	
	@Then("^user clicks on agree button$")
	public void agree() {
	BusinessFunctions.Click(LoginPF.agree());
	}
	
	@Then("^user clicks no on agreement page$")
	public void do_not_agree() {
	BusinessFunctions.Click(LoginPF.decline());
	}

	@Then("^user enters incorrect username and incorrect password$")
	public void user_enters_incorrect_username_and_incorrect_password() throws InterruptedException {
		BusinessFunctions.Sendkeys(LoginPF.username(),"varun");
		BusinessFunctions.Sendkeys(LoginPF.password(),"kumar");

	}
	
	@Then("^user enters nothing in username and correct password$")
	public void user_enters_nothing_in_username_and_correct_password()  {
		BusinessFunctions.Sendkeys(LoginPF.username(),"");
		BusinessFunctions.Sendkeys(LoginPF.password(),"test1234");

	}

	@Then("^user enters incorrect username and nothing in password$")
	public void user_enters_incorrect_username_and_nothing_in_password()  {
		BusinessFunctions.Sendkeys(LoginPF.username(),"qadmuser6");
		BusinessFunctions.Sendkeys(LoginPF.password(),"");


	}
	
	@Then("^user shows an invalid message username can not be empty$")
	public void user_shows_an_invalid_message_username_can_not_be_empty() {
	   BusinessFunctions.Assert_String_Case("User ID is a required field.",BusinessFunctions.getText(LoginPF.loginStatus()));
	}

	@Then("^user shows an invalid message password can not be empty$")
	public void user_shows_an_invalid_message_password_can_not_be_empty()  {
	   BusinessFunctions.Assert_String_Case("Password is a required field.",BusinessFunctions.getText(LoginPF.loginStatus()));

	}
	
	@Then("^user enters \"([^\"]*)\" and \"([^\"]*)\"$")
	public void user_enters_and(String username, String password) throws InterruptedException {
		username =App_Username;
        password = App_Password;
		BusinessFunctions.Sendkeys(LoginPF.username(),username);
		BusinessFunctions.Sendkeys(LoginPF.password(),password);
        Thread.sleep(2000);


	}
	@Then("^close browser$")
	public void close_browser() throws TestLinkAPIException, IOException{
        BusinessFunctions.clearField(LoginPF.username());
	}
	
	@Then("^close browser with testid \"([^\"]*)\"$")
	public void close_browser_with_testid(String arg1) throws IOException, TestLinkAPIException, InterruptedException {
		
		Thread.sleep(2000);
		System.out.println(BusinessFunctions.getTitle());
        if( BusinessFunctions.getTitle().contains("InvestTrade iWeb"))
		        BusinessFunctions.Click(LoginPF.signOut());
        else
                BusinessFunctions.clearField(LoginPF.username());

	}
	
	public static String testName()
	{
		return testno;
	}
	
	
	@Then("^user enters \"([^\"]*)\" and \"([^\"]*)\" and submit$")
	public void loginProcedure(String username, String password) throws Exception {

        username =App_Username;
        password = App_Password;
		BusinessFunctions.Sendkeys(LoginPF.username(), username);
		BusinessFunctions.Sendkeys(LoginPF.password(), password);
		BusinessFunctions.Click(LoginPF.submit());
		Thread.sleep(2000);
        BusinessFunctions.Click(LoginPF.agree());
        Thread.sleep(6000);
	}

	
	@Then("^title of login page is Infocast with testid \"([^\"]*)\"$")
	public void title_of_login_page_is_Infocast_with_testid(String arg1) {

		Hooks.testid = arg1;
		BusinessFunctions.Assert_String_Case("Infocast",BusinessFunctions.getTitle());
	}

	@Then("^user shows an invalid message with testid \"([^\"]*)\"$")
	public void user_shows_an_invalid_message_with_testid(String arg1) {


		boolean text =  BusinessFunctions.getText(LoginPF.loginStatus()).contains("Invalid User ID or password");
		BusinessFunctions.AssertTrue_Boolean(text);
	}

	@Then("^user shows an invalid message username can not be empty with testid \"([^\"]*)\"$")
	public void user_shows_an_invalid_message_username_can_not_be_empty_with_testid(String arg1) {
		

		 BusinessFunctions.Assert_String_Case("User ID is a required field.",BusinessFunctions.getText(LoginPF.loginStatus()));
		
	}

	@Then("^user shows an invalid message password can not be empty with testid \"([^\"]*)\"$")
	public void user_shows_an_invalid_message_password_can_not_be_empty_with_testid(String arg1) {
		

		BusinessFunctions.Assert_String_Case("Password is a required field.",BusinessFunctions.getText(LoginPF.loginStatus()));
	}


	/*@Given("^Verify Testcase with testid \"([^\"]*)\"$")
	public void verify_Testcase_with_testid(String testcaseID) {
		Hooks.testid = testcaseID;
	}
*/
}
