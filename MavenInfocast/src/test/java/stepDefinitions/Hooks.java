package stepDefinitions;

import java.io.IOException;

import PageObjectModel.BusinessFunctions;
import junit.runner.Version;
import org.apache.http.TruncatedChunkException;
import org.openqa.selenium.Platform;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.firefox.internal.ProfilesIni;
import org.openqa.selenium.ie.InternetExplorerDriver;
import br.eti.kinoshita.testlinkjavaapi.util.TestLinkAPIException;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import stepDefinitions.ChangePassword;
import testlink.api.java.client.TestLinkAPIClient;
import testlink.api.java.client.TestLinkAPIHelper;
import testlink.api.java.client.TestLinkAPIResults;
import utility.TestlinkIntegration;
import org.openqa.selenium.WebDriver.*;


public class Hooks {

    public static String testlinkURL = System.getProperty("testlink_url");

    /*TestLinkAPIHelper help = new TestLinkAPIHelper();

	Protected WebDriver driver = null ;
	static String browsername = System.getProperty("browser_name");*/
	protected static WebDriver driver = null;
	public static String browser = System.getProperty("browser_name");
    public static String testid;
    TestLinkAPIHelper help = new TestLinkAPIHelper();
    TestLinkAPIClient testlink = new TestLinkAPIClient("daa88d6c650efc6013f88fbcbd64fc77",testlinkURL);

    @Before
	public static WebDriver initDriver() {
		System.out.println(browser);
		if (driver == null) {
			if ("FF".equalsIgnoreCase(browser)) {
				System.setProperty("webdriver.gecko.driver", System.getProperty("user.dir") + "\\lib\\driver32\\geckodriver.exe");
				DesiredCapabilities dc = DesiredCapabilities.firefox();
				dc.setCapability("marionette", false);
				dc.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
				dc.setCapability("disable-restore-session-state",true);
				driver = new FirefoxDriver(dc);


			} else if ("IE".equalsIgnoreCase(browser)) {

				System.setProperty("webdriver.ie.driver", System.getProperty("user.dir") + "\\lib\\IEDriverServer.exe");
				driver = new InternetExplorerDriver();
			} else if ("Chrome".equalsIgnoreCase(browser)) {

				System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir") + "\\lib\\chromeP\\chromedriver.exe");
				driver = new ChromeDriver();

			} else {
				// default Chrome
				System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir") + "\\lib\\chromeP\\chromedriver.exe");
				driver = new ChromeDriver();

			}
		}
		return driver;
	}


	@After
    public void afterScenario(Scenario scenario) throws IOException, TestLinkAPIException, testlink.api.java.client.TestLinkAPIException{

		System.out.println("++++++++After Hook++++++++");
        System.out.println(testid);
		if(scenario.isFailed())
			{
				BusinessFunctions.captureScreenshot();
				System.out.println("Failed");
				TestlinkIntegration.updateResults(testid, "FAIL" ,TestLinkAPIResults.TEST_FAILED);

			}
			else
			{
			
				System.out.println("Passed");
				TestlinkIntegration.updateResults(testid, "Pass" ,TestLinkAPIResults.TEST_PASSED);
		
			}

 }
}
