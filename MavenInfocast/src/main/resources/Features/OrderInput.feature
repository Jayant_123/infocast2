Feature: ordering stock



@orderInput@regression1
Scenario Outline: TestCaseId- "<TestCaseId>" - To verify Order Input with "<Scenario>"

Given Verify Testcase with testid "<TestCaseId>"
Given user logged with "username" and "password" and submit
When user click on buy icon
Then user select type of market as "<market>"
And user enter stock code as "<stockcode>"
And  order price as "<orderprice>"
And order quantity as "<quantity>"
And order type as "<ordertype>"
And order term as "<orderterm>"
Then Preview order
Then message given is "<message>" with  testcase"<TestCaseId>"
Then close the current browser 

Examples:
| TestCaseId	| market | stockcode | orderprice | quantity | ordertype     		| orderterm  		   | message			         										|  Scenario     									| 
|  IWEB-18 		|  SZMK	 | 			 |            |          | Limit Order			| Good for today       | Stock Code cannot be empty.      									|  All blank values	in SZMK							|
|  IWEB-19 	    |  SZMK	 | 2		 |            |          | Limit Order		    | Good for today       | Stock code 00002 is invalid.   									|  order price and quantity empty in SZMK			|
|  IWEB-20 	    |  SZMK	 | 2 		 | 23.001     |          | Limit Order			| Good for today       | Quantity cannot be zero or empty. 									|  quantity only empty in SZMK						|


