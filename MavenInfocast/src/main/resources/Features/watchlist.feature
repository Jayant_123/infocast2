Feature: WatchList test

@watchlisttest
Scenario Outline: Test id - "<Testid>" Stock code checking

Given user logged with "username" and "password" and submit..
When user clicks on market information
Then user selects SEHK watchlist
When user enters stock code "<stockcode>"
And click on add
Then stock table gets updated with "<stockcode>"
Then close the current browser..
Examples:

|    Testid    |    stockcode    |
|		1	   |	 00002	 |
|		2	   |	 00048	 |
|		3	   |	 00032	 |
|		3	   |	 00032	 |
|		3	   |	 00032	 |
|		3	   |	 00032	 |
|		3	   |	 00032	 |
|		3	   |	 00032	 |
|		3	   |	 00032	 |
|		3	   |	 00032	 |
|		3	   |	 00032	 |
|		3	   |	 00032	 |
|		3	   |	 00032	 |
|		3	   |	 00032	 |
|		3	   |	 00032	 |
|		3	   |	 00032	 |
|		3	   |	 00032	 |
|		3	   |	 00032	 |
