$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("Features/changePassword.feature");
formatter.feature({
  "line": 1,
  "name": "Settings change password",
  "description": "",
  "id": "settings-change-password",
  "keyword": "Feature"
});
formatter.scenarioOutline({
  "line": 5,
  "name": "TestCaseId- \"\u003cTestCaseId\u003e\" Credentials test with \"\u003cScenario\u003e\"",
  "description": "",
  "id": "settings-change-password;testcaseid--\"\u003ctestcaseid\u003e\"-credentials-test-with-\"\u003cscenario\u003e\"",
  "type": "scenario_outline",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "line": 4,
      "name": "@changePassword"
    },
    {
      "line": 4,
      "name": "@regression1"
    },
    {
      "line": 4,
      "name": "@P1"
    },
    {
      "line": 4,
      "name": "@P2"
    }
  ]
});
formatter.step({
  "line": 6,
  "name": "Verify Testcase with testid \"\u003cTestCaseId\u003e\"",
  "keyword": "Given "
});
formatter.step({
  "line": 7,
  "name": "user enters \"username\" and \"password\" and submit..",
  "keyword": "Given "
});
formatter.step({
  "line": 8,
  "name": "User should be on homepage",
  "keyword": "Then "
});
formatter.step({
  "line": 9,
  "name": "User clicks on My setting and change password",
  "keyword": "Then "
});
formatter.step({
  "line": 10,
  "name": "redirect to change password page",
  "keyword": "Then "
});
formatter.step({
  "line": 11,
  "name": "user enters Old Password as \"\u003cold password\u003e\"",
  "keyword": "When "
});
formatter.step({
  "line": 12,
  "name": "user enters New password as \"\u003cnew password\u003e\"",
  "keyword": "And "
});
formatter.step({
  "line": 13,
  "name": "user enter Confirm password as \"\u003cconfirm new password\u003e\"",
  "keyword": "And "
});
formatter.step({
  "line": 14,
  "name": "user clicks on save",
  "keyword": "Then "
});
formatter.step({
  "line": 15,
  "name": "error message given is \"\u003cmessage\u003e\" with testid \"\u003cTestCaseId\u003e\"",
  "keyword": "And "
});
formatter.step({
  "line": 16,
  "name": "close the browser",
  "keyword": "And "
});
formatter.examples({
  "line": 18,
  "name": "",
  "description": "",
  "id": "settings-change-password;testcaseid--\"\u003ctestcaseid\u003e\"-credentials-test-with-\"\u003cscenario\u003e\";",
  "rows": [
    {
      "cells": [
        "TestCaseId",
        "old password",
        "new password",
        "confirm new password",
        "message",
        "Scenario"
      ],
      "line": 20,
      "id": "settings-change-password;testcaseid--\"\u003ctestcaseid\u003e\"-credentials-test-with-\"\u003cscenario\u003e\";;1"
    },
    {
      "cells": [
        "IWEB-9",
        "asd",
        "qwerty1234",
        "qwerty1234",
        "The old password is incorrect.",
        "incorrect old password"
      ],
      "line": 21,
      "id": "settings-change-password;testcaseid--\"\u003ctestcaseid\u003e\"-credentials-test-with-\"\u003cscenario\u003e\";;2"
    },
    {
      "cells": [
        "IWEB-10",
        "",
        "qwerty1234",
        "qwerty1234",
        "Please enter value into the following required field(s): Old Password",
        "incorrect new password"
      ],
      "line": 22,
      "id": "settings-change-password;testcaseid--\"\u003ctestcaseid\u003e\"-credentials-test-with-\"\u003cscenario\u003e\";;3"
    }
  ],
  "keyword": "Examples"
});
formatter.before({
  "duration": 8574973686,
  "status": "passed"
});
formatter.scenario({
  "line": 21,
  "name": "TestCaseId- \"IWEB-9\" Credentials test with \"incorrect old password\"",
  "description": "",
  "id": "settings-change-password;testcaseid--\"\u003ctestcaseid\u003e\"-credentials-test-with-\"\u003cscenario\u003e\";;2",
  "type": "scenario",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "line": 4,
      "name": "@regression1"
    },
    {
      "line": 4,
      "name": "@P1"
    },
    {
      "line": 4,
      "name": "@P2"
    },
    {
      "line": 4,
      "name": "@changePassword"
    }
  ]
});
formatter.step({
  "line": 6,
  "name": "Verify Testcase with testid \"IWEB-9\"",
  "matchedColumns": [
    0
  ],
  "keyword": "Given "
});
formatter.step({
  "line": 7,
  "name": "user enters \"username\" and \"password\" and submit..",
  "keyword": "Given "
});
formatter.step({
  "line": 8,
  "name": "User should be on homepage",
  "keyword": "Then "
});
formatter.step({
  "line": 9,
  "name": "User clicks on My setting and change password",
  "keyword": "Then "
});
formatter.step({
  "line": 10,
  "name": "redirect to change password page",
  "keyword": "Then "
});
formatter.step({
  "line": 11,
  "name": "user enters Old Password as \"asd\"",
  "matchedColumns": [
    1
  ],
  "keyword": "When "
});
formatter.step({
  "line": 12,
  "name": "user enters New password as \"qwerty1234\"",
  "matchedColumns": [
    2
  ],
  "keyword": "And "
});
formatter.step({
  "line": 13,
  "name": "user enter Confirm password as \"qwerty1234\"",
  "matchedColumns": [
    3
  ],
  "keyword": "And "
});
formatter.step({
  "line": 14,
  "name": "user clicks on save",
  "keyword": "Then "
});
formatter.step({
  "line": 15,
  "name": "error message given is \"The old password is incorrect.\" with testid \"IWEB-9\"",
  "matchedColumns": [
    0,
    4
  ],
  "keyword": "And "
});
formatter.step({
  "line": 16,
  "name": "close the browser",
  "keyword": "And "
});
formatter.match({
  "arguments": [
    {
      "val": "IWEB-9",
      "offset": 29
    }
  ],
  "location": "ChangePassword.verify_Testcase_with_testid(String)"
});
formatter.result({
  "duration": 203865253,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "username",
      "offset": 13
    },
    {
      "val": "password",
      "offset": 28
    }
  ],
  "location": "ChangePassword.loginProcedure(String,String)"
});
formatter.result({
  "duration": 10747275760,
  "status": "passed"
});
formatter.match({
  "location": "ChangePassword.user_already_on_home()"
});
formatter.result({
  "duration": 5063857502,
  "status": "passed"
});
formatter.match({
  "location": "ChangePassword.user_clicks_on_My_setting_and_change_password()"
});
formatter.result({
  "duration": 8294277945,
  "status": "passed"
});
formatter.match({
  "location": "ChangePassword.redirect_to_change_password_page()"
});
formatter.result({
  "duration": 6134472648,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "asd",
      "offset": 29
    }
  ],
  "location": "ChangePassword.user_enters(String)"
});
formatter.result({
  "duration": 3079080983,
  "error_message": "org.openqa.selenium.NoSuchElementException: Unable to locate element: {\"method\":\"id\",\"selector\":\"changePasswordForm:j_id18\"}\nCommand duration or timeout: 0 milliseconds\r\n\tat sun.reflect.NativeConstructorAccessorImpl.newInstance0(Native Method)\r\n\tat sun.reflect.NativeConstructorAccessorImpl.newInstance(NativeConstructorAccessorImpl.java:62)\r\n\tat sun.reflect.DelegatingConstructorAccessorImpl.newInstance(DelegatingConstructorAccessorImpl.java:45)\r\n\tat java.lang.reflect.Constructor.newInstance(Constructor.java:423)\r\n\tat org.openqa.selenium.remote.ErrorHandler.createThrowable(ErrorHandler.java:215)\r\n\tat org.openqa.selenium.remote.ErrorHandler.throwIfResponseFailed(ErrorHandler.java:167)\r\n\tat org.openqa.selenium.remote.http.JsonHttpResponseCodec.reconstructValue(JsonHttpResponseCodec.java:40)\r\n\tat org.openqa.selenium.remote.http.AbstractHttpResponseCodec.decode(AbstractHttpResponseCodec.java:82)\r\n\tat org.openqa.selenium.remote.http.AbstractHttpResponseCodec.decode(AbstractHttpResponseCodec.java:45)\r\n\tat org.openqa.selenium.remote.HttpCommandExecutor.execute(HttpCommandExecutor.java:164)\r\n\tat org.openqa.selenium.remote.service.DriverCommandExecutor.execute(DriverCommandExecutor.java:82)\r\n\tat org.openqa.selenium.remote.RemoteWebDriver.execute(RemoteWebDriver.java:646)\r\n\tat org.openqa.selenium.remote.RemoteWebDriver.findElement(RemoteWebDriver.java:416)\r\n\tat org.openqa.selenium.remote.RemoteWebDriver.findElementById(RemoteWebDriver.java:462)\r\n\tat org.openqa.selenium.By$ById.findElement(By.java:218)\r\n\tat org.openqa.selenium.remote.RemoteWebDriver.findElement(RemoteWebDriver.java:408)\r\n\tat PageObjectModel.ChangePassPage.oldPassword(ChangePassPage.java:46)\r\n\tat stepDefinitions.ChangePassword.user_enters(ChangePassword.java:82)\r\n\tat ✽.When user enters Old Password as \"asd\"(Features/changePassword.feature:11)\r\nCaused by: org.openqa.selenium.NoSuchElementException: Unable to locate element: {\"method\":\"id\",\"selector\":\"changePasswordForm:j_id18\"}\nFor documentation on this error, please visit: http://seleniumhq.org/exceptions/no_such_element.html\nBuild info: version: \u00273.5.3\u0027, revision: \u0027a88d25fe6b\u0027, time: \u00272017-08-29T12:42:44.417Z\u0027\nSystem info: host: \u0027RLE0584\u0027, ip: \u0027192.168.150.124\u0027, os.name: \u0027Windows 7\u0027, os.arch: \u0027amd64\u0027, os.version: \u00276.1\u0027, java.version: \u00271.8.0_171\u0027\nDriver info: driver.version: unknown\r\n\tat \u003canonymous class\u003e.FirefoxDriver.prototype.findElementInternal_(file:///C:/Users/rle0584/AppData/Local/Temp/anonymous6738137185838035085webdriver-profile/extensions/fxdriver@googlecode.com/components/driver-component.js:11867)\r\n\tat \u003canonymous class\u003e.FirefoxDriver.prototype.findElement(file:///C:/Users/rle0584/AppData/Local/Temp/anonymous6738137185838035085webdriver-profile/extensions/fxdriver@googlecode.com/components/driver-component.js:11876)\r\n\tat \u003canonymous class\u003e.DelayedCommand.prototype.executeInternal_/k(file:///C:/Users/rle0584/AppData/Local/Temp/anonymous6738137185838035085webdriver-profile/extensions/fxdriver@googlecode.com/components/command-processor.js:13353)\r\n\tat \u003canonymous class\u003e.DelayedCommand.prototype.executeInternal_(file:///C:/Users/rle0584/AppData/Local/Temp/anonymous6738137185838035085webdriver-profile/extensions/fxdriver@googlecode.com/components/command-processor.js:13358)\r\n\tat \u003canonymous class\u003e.DelayedCommand.prototype.execute/\u003c(file:///C:/Users/rle0584/AppData/Local/Temp/anonymous6738137185838035085webdriver-profile/extensions/fxdriver@googlecode.com/components/command-processor.js:13300)\r\n",
  "status": "failed"
});
formatter.match({
  "arguments": [
    {
      "val": "qwerty1234",
      "offset": 29
    }
  ],
  "location": "ChangePassword.user_enters_tesr(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "arguments": [
    {
      "val": "qwerty1234",
      "offset": 32
    }
  ],
  "location": "ChangePassword.user_enter_confirm_new_password(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "location": "ChangePassword.user_clicks_on_save()"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "arguments": [
    {
      "val": "The old password is incorrect.",
      "offset": 24
    },
    {
      "val": "IWEB-9",
      "offset": 69
    }
  ],
  "location": "ChangePassword.error_message_given_is_with_testid(String,String)"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "location": "ChangePassword.close_the_browser()"
});
formatter.result({
  "status": "skipped"
});
