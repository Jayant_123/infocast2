Feature: Settings change password 


@changePassword@regression1@P1@P2
Scenario Outline: TestCaseId- "<TestCaseId>" Credentials test with "<Scenario>"
Given Verify Testcase with testid "<TestCaseId>"
Given user enters "username" and "password" and submit..
Then User should be on homepage
Then User clicks on My setting and change password
Then redirect to change password page
When user enters Old Password as "<old password>" 
And user enters New password as "<new password>"
And user enter Confirm password as "<confirm new password>"
Then user clicks on save
And error message given is "<message>" with testid "<TestCaseId>"
And close the browser

Examples:

| TestCaseId|old password|new password		   |confirm new password	  |message																										  | Scenario     									| 											
| IWEB-9	|asd		 |qwerty1234		   |qwerty1234                |The old password is incorrect.																				  	  | incorrect old password  						|
| IWEB-10	|            |qwerty1234		   |qwerty1234				  |Please enter value into the following required field(s): Old Password						   			  | incorrect new password   						|

