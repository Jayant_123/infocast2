Feature: Infocast Login Feature

@login@regression1@P1@P2
Scenario: Testid- IWEB-1 Infocast Login Test Scenario
Given Verify Testcase with testid "IWEB-1"
Given user is already on Login Page
When title of login page is Infocast
Then user enters "username" and "password" and submit
Then user is on home page with testid "IWEB-1"
Then close browser with testid "IWEB-1"

@login@regression1@P1@P2
Scenario: Testid- IWEB-2 Infocast Login Test Scenario
Given Verify Testcase with testid "IWEB-2"
Given user is already on Login Page
When title of login page is Infocast
Then user enters "username" and "password" and submit
Then user is on home page with testid "IWEB-2"
Then close browser with testid "IWEB-2"

@login@regression1@P1
Scenario: Testid- IWEB-3 Infocast Login Test agreement(no)
Given Verify Testcase with testid "IWEB-3"
Given user is already on Login Page
When title of login page is Infocast
Then user enters "qadmuser7" and "test1234"
Then user clicks on submit button
Then user clicks no on agreement page
Then title of login page is Infocast with testid "IWEB-3"
Then close browser with testid "IWEB-3"
@login@regression1@P1
Scenario: Testid- IWEB-4 Infocast login with incorrect username
Given Verify Testcase with testid "IWEB-4"
Given user is already on Login Page
When title of login page is Infocast
Then user enters incorrect username and correct password
Then user clicks on submit button
Then user shows an invalid message with testid "IWEB-4"
Then close browser with testid "IWEB-4"
@login@regression1
Scenario: Testid- IWEB-5 Infocast login with incorrect password
Given Verify Testcase with testid "IWEB-5"
Given user is already on Login Page
When title of login page is Infocast
Then user enters correct username and incorrect password
Then user clicks on submit button
Then user shows an invalid message with testid "IWEB-5"
Then close browser with testid "IWEB-5"
@login@regression1
Scenario: Testid- IWEB-6 Infocast login with incorrect username and incorrect password
Given Verify Testcase with testid "IWEB-6"
Given user is already on Login Page
When title of login page is Infocast
Then user enters incorrect username and incorrect password
Then user clicks on submit button
Then user shows an invalid message with testid "IWEB-6"
Then close browser with testid "IWEB-6"
@login@regression1
Scenario: Testid- IWEB-7 Infocast login with blank username
Given Verify Testcase with testid "IWEB-7"
Given user is already on Login Page
When title of login page is Infocast
Then user enters nothing in username and correct password
Then user clicks on submit button
Then user shows an invalid message username can not be empty with testid "IWEB-7"
Then close browser with testid "IWEB-7"
@login@regression1
Scenario: Testid- IWEB-8 Infocast login with blank password
Given Verify Testcase with testid "IWEB-8"
Given user is already on Login Page
When title of login page is Infocast
Then user enters incorrect username and nothing in password
Then user clicks on submit button
Then user shows an invalid message password can not be empty with testid "IWEB-8"
Then close browser with testid "IWEB-8"

