$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("OrderInput.feature");
formatter.feature({
  "line": 1,
  "name": "ordering stock",
  "description": "",
  "id": "ordering-stock",
  "keyword": "Feature"
});
formatter.scenarioOutline({
  "line": 145,
  "name": "TestCaseId- \"\u003cTestCaseId\u003e\" - To verify Order Input with \"\u003cScenario\u003e\" rest",
  "description": "",
  "id": "ordering-stock;testcaseid--\"\u003ctestcaseid\u003e\"---to-verify-order-input-with-\"\u003cscenario\u003e\"-rest",
  "type": "scenario_outline",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "line": 144,
      "name": "@smoke2"
    },
    {
      "line": 144,
      "name": "@test2"
    }
  ]
});
formatter.step({
  "line": 146,
  "name": "user logged with \"username\" and \"password\" and submit",
  "keyword": "Given "
});
formatter.step({
  "line": 147,
  "name": "user click on buy icon",
  "keyword": "When "
});
formatter.step({
  "line": 148,
  "name": "user select type of market as \"\u003cmarket\u003e\"",
  "keyword": "Then "
});
formatter.step({
  "line": 149,
  "name": "user enter stock code as \"\u003cstockcode\u003e\"",
  "keyword": "And "
});
formatter.step({
  "line": 150,
  "name": "order price as \"\u003corderprice\u003e\"",
  "keyword": "And "
});
formatter.step({
  "line": 151,
  "name": "order quantity as \"\u003cquantity\u003e\"",
  "keyword": "And "
});
formatter.step({
  "line": 152,
  "name": "order type as \"\u003cordertype\u003e\"",
  "keyword": "And "
});
formatter.step({
  "line": 153,
  "name": "order term as \"\u003corderterm\u003e\"",
  "keyword": "And "
});
formatter.step({
  "line": 154,
  "name": "Preview order",
  "keyword": "Then "
});
formatter.step({
  "line": 155,
  "name": "message given is \"\u003cmessage\u003e\"",
  "keyword": "Then "
});
formatter.step({
  "line": 156,
  "name": "close the current browser",
  "keyword": "Then "
});
formatter.examples({
  "line": 158,
  "name": "",
  "description": "",
  "id": "ordering-stock;testcaseid--\"\u003ctestcaseid\u003e\"---to-verify-order-input-with-\"\u003cscenario\u003e\"-rest;",
  "rows": [
    {
      "cells": [
        "TestCaseId",
        "market",
        "stockcode",
        "orderprice",
        "quantity",
        "ordertype",
        "orderterm",
        "message",
        "Scenario"
      ],
      "line": 159,
      "id": "ordering-stock;testcaseid--\"\u003ctestcaseid\u003e\"---to-verify-order-input-with-\"\u003cscenario\u003e\"-rest;;1"
    },
    {
      "cells": [
        "IWEB_SZMK_277",
        "SZMK",
        "2",
        "23.000",
        "123",
        "Limit Order",
        "Good for today",
        "Quantity not a valid lot size [146]",
        "incorrect limit price in SZMK"
      ],
      "line": 160,
      "id": "ordering-stock;testcaseid--\"\u003ctestcaseid\u003e\"---to-verify-order-input-with-\"\u003cscenario\u003e\"-rest;;2"
    },
    {
      "cells": [
        "IWEB_SZMK_278",
        "SZMK",
        "2",
        "23.000",
        "100",
        "Limit Order",
        "Good for today",
        "SZMK buy order price violates max 3% change limit. [1515]",
        "incorrect limit price in SZMK"
      ],
      "line": 161,
      "id": "ordering-stock;testcaseid--\"\u003ctestcaseid\u003e\"---to-verify-order-input-with-\"\u003cscenario\u003e\"-rest;;3"
    },
    {
      "cells": [
        "IWEB_SZMK_279",
        "SZMK",
        "2",
        "23.000",
        "1000",
        "Limit Order",
        "Good for today",
        "SZMK buy order price violates max 3% change limit. [1515]",
        "incorrect limit price in SZMK"
      ],
      "line": 162,
      "id": "ordering-stock;testcaseid--\"\u003ctestcaseid\u003e\"---to-verify-order-input-with-\"\u003cscenario\u003e\"-rest;;4"
    },
    {
      "cells": [
        "IWEB_SZMK_226",
        "SZMK",
        "8",
        "4.400",
        "1000",
        "Limit Order",
        "Good for today",
        "SZMK buy order price violates max 3% change limit. [1515]",
        "incorrect quantity for given stock code in SZMK"
      ],
      "line": 163,
      "id": "ordering-stock;testcaseid--\"\u003ctestcaseid\u003e\"---to-verify-order-input-with-\"\u003cscenario\u003e\"-rest;;5"
    },
    {
      "cells": [
        "IWEB_SZMK_228",
        "SZMK",
        "00052",
        "11.310",
        "500",
        "Limit Order",
        "Good for today",
        "Stock code 00052 is invalid.",
        "invalid stock code in SZMK"
      ],
      "line": 164,
      "id": "ordering-stock;testcaseid--\"\u003ctestcaseid\u003e\"---to-verify-order-input-with-\"\u003cscenario\u003e\"-rest;;6"
    }
  ],
  "keyword": "Examples"
});
formatter.scenario({
  "line": 160,
  "name": "TestCaseId- \"IWEB_SZMK_277\" - To verify Order Input with \"incorrect limit price in SZMK\" rest",
  "description": "",
  "id": "ordering-stock;testcaseid--\"\u003ctestcaseid\u003e\"---to-verify-order-input-with-\"\u003cscenario\u003e\"-rest;;2",
  "type": "scenario",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "line": 144,
      "name": "@smoke2"
    },
    {
      "line": 144,
      "name": "@test2"
    }
  ]
});
formatter.step({
  "line": 146,
  "name": "user logged with \"username\" and \"password\" and submit",
  "keyword": "Given "
});
formatter.step({
  "line": 147,
  "name": "user click on buy icon",
  "keyword": "When "
});
formatter.step({
  "line": 148,
  "name": "user select type of market as \"SZMK\"",
  "matchedColumns": [
    1
  ],
  "keyword": "Then "
});
formatter.step({
  "line": 149,
  "name": "user enter stock code as \"2\"",
  "matchedColumns": [
    2
  ],
  "keyword": "And "
});
formatter.step({
  "line": 150,
  "name": "order price as \"23.000\"",
  "matchedColumns": [
    3
  ],
  "keyword": "And "
});
formatter.step({
  "line": 151,
  "name": "order quantity as \"123\"",
  "matchedColumns": [
    4
  ],
  "keyword": "And "
});
formatter.step({
  "line": 152,
  "name": "order type as \"Limit Order\"",
  "matchedColumns": [
    5
  ],
  "keyword": "And "
});
formatter.step({
  "line": 153,
  "name": "order term as \"Good for today\"",
  "matchedColumns": [
    6
  ],
  "keyword": "And "
});
formatter.step({
  "line": 154,
  "name": "Preview order",
  "keyword": "Then "
});
formatter.step({
  "line": 155,
  "name": "message given is \"Quantity not a valid lot size [146]\"",
  "matchedColumns": [
    7
  ],
  "keyword": "Then "
});
formatter.step({
  "line": 156,
  "name": "close the current browser",
  "keyword": "Then "
});
formatter.match({
  "arguments": [
    {
      "val": "username",
      "offset": 18
    },
    {
      "val": "password",
      "offset": 33
    }
  ],
  "location": "OrderInputSD.user_logged_in_to_the_application(String,String)"
});
formatter.result({
  "duration": 14962008148,
  "status": "passed"
});
formatter.match({
  "location": "OrderInputSD.user_click_on_buy_icon()"
});
formatter.result({
  "duration": 9073908199,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "SZMK",
      "offset": 31
    }
  ],
  "location": "OrderInputSD.user_select_type_of_market_as(String)"
});
formatter.result({
  "duration": 292659271,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "2",
      "offset": 26
    }
  ],
  "location": "OrderInputSD.user_enter_stock_code_as(String)"
});
formatter.result({
  "duration": 239930069,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "23.000",
      "offset": 16
    }
  ],
  "location": "OrderInputSD.order_price_as(String)"
});
formatter.result({
  "duration": 480138425,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "123",
      "offset": 19
    }
  ],
  "location": "OrderInputSD.order_quantity_as(String)"
});
formatter.result({
  "duration": 245251068,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Limit Order",
      "offset": 15
    }
  ],
  "location": "OrderInputSD.order_type_as(String)"
});
formatter.result({
  "duration": 300677086,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Good for today",
      "offset": 15
    }
  ],
  "location": "OrderInputSD.order_term_as(String)"
});
formatter.result({
  "duration": 315824169,
  "status": "passed"
});
formatter.match({
  "location": "OrderInputSD.preview_order()"
});
formatter.result({
  "duration": 316278506,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Quantity not a valid lot size [146]",
      "offset": 18
    }
  ],
  "location": "OrderInputSD.error_message_given_is(String)"
});
formatter.result({
  "duration": 756573387,
  "status": "passed"
});
formatter.match({
  "location": "OrderInputSD.close_the_current_browser()"
});
formatter.result({
  "duration": 749247146,
  "status": "passed"
});
formatter.after({
  "duration": 369470,
  "status": "passed"
});
formatter.after({
  "duration": 63552,
  "status": "passed"
});
formatter.after({
  "duration": 176841,
  "status": "passed"
});
formatter.scenario({
  "line": 161,
  "name": "TestCaseId- \"IWEB_SZMK_278\" - To verify Order Input with \"incorrect limit price in SZMK\" rest",
  "description": "",
  "id": "ordering-stock;testcaseid--\"\u003ctestcaseid\u003e\"---to-verify-order-input-with-\"\u003cscenario\u003e\"-rest;;3",
  "type": "scenario",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "line": 144,
      "name": "@smoke2"
    },
    {
      "line": 144,
      "name": "@test2"
    }
  ]
});
formatter.step({
  "line": 146,
  "name": "user logged with \"username\" and \"password\" and submit",
  "keyword": "Given "
});
formatter.step({
  "line": 147,
  "name": "user click on buy icon",
  "keyword": "When "
});
formatter.step({
  "line": 148,
  "name": "user select type of market as \"SZMK\"",
  "matchedColumns": [
    1
  ],
  "keyword": "Then "
});
formatter.step({
  "line": 149,
  "name": "user enter stock code as \"2\"",
  "matchedColumns": [
    2
  ],
  "keyword": "And "
});
formatter.step({
  "line": 150,
  "name": "order price as \"23.000\"",
  "matchedColumns": [
    3
  ],
  "keyword": "And "
});
formatter.step({
  "line": 151,
  "name": "order quantity as \"100\"",
  "matchedColumns": [
    4
  ],
  "keyword": "And "
});
formatter.step({
  "line": 152,
  "name": "order type as \"Limit Order\"",
  "matchedColumns": [
    5
  ],
  "keyword": "And "
});
formatter.step({
  "line": 153,
  "name": "order term as \"Good for today\"",
  "matchedColumns": [
    6
  ],
  "keyword": "And "
});
formatter.step({
  "line": 154,
  "name": "Preview order",
  "keyword": "Then "
});
formatter.step({
  "line": 155,
  "name": "message given is \"SZMK buy order price violates max 3% change limit. [1515]\"",
  "matchedColumns": [
    7
  ],
  "keyword": "Then "
});
formatter.step({
  "line": 156,
  "name": "close the current browser",
  "keyword": "Then "
});
formatter.match({
  "arguments": [
    {
      "val": "username",
      "offset": 18
    },
    {
      "val": "password",
      "offset": 33
    }
  ],
  "location": "OrderInputSD.user_logged_in_to_the_application(String,String)"
});
formatter.result({
  "duration": 10176008611,
  "status": "passed"
});
formatter.match({
  "location": "OrderInputSD.user_click_on_buy_icon()"
});
formatter.result({
  "duration": 8690716160,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "SZMK",
      "offset": 31
    }
  ],
  "location": "OrderInputSD.user_select_type_of_market_as(String)"
});
formatter.result({
  "duration": 278437048,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "2",
      "offset": 26
    }
  ],
  "location": "OrderInputSD.user_enter_stock_code_as(String)"
});
formatter.result({
  "duration": 244651468,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "23.000",
      "offset": 16
    }
  ],
  "location": "OrderInputSD.order_price_as(String)"
});
formatter.result({
  "duration": 515135834,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "100",
      "offset": 19
    }
  ],
  "location": "OrderInputSD.order_quantity_as(String)"
});
formatter.result({
  "duration": 262782732,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Limit Order",
      "offset": 15
    }
  ],
  "location": "OrderInputSD.order_type_as(String)"
});
formatter.result({
  "duration": 356110208,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Good for today",
      "offset": 15
    }
  ],
  "location": "OrderInputSD.order_term_as(String)"
});
formatter.result({
  "duration": 297731589,
  "status": "passed"
});
formatter.match({
  "location": "OrderInputSD.preview_order()"
});
formatter.result({
  "duration": 282467269,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "SZMK buy order price violates max 3% change limit. [1515]",
      "offset": 18
    }
  ],
  "location": "OrderInputSD.error_message_given_is(String)"
});
formatter.result({
  "duration": 759653092,
  "error_message": "junit.framework.ComparisonFailure: expected:\u003c[SZMK buy order price violates max 3% change limit. [1515]]\u003e but was:\u003c[Do not allow to buy this product [10013]]\u003e\r\n\tat junit.framework.Assert.assertEquals(Assert.java:100)\r\n\tat junit.framework.Assert.assertEquals(Assert.java:107)\r\n\tat stepDefinitions.OrderInputSD.error_message_given_is(OrderInputSD.java:175)\r\n\tat ✽.Then message given is \"SZMK buy order price violates max 3% change limit. [1515]\"(OrderInput.feature:155)\r\n",
  "status": "failed"
});
formatter.match({
  "location": "OrderInputSD.close_the_current_browser()"
});
formatter.result({
  "status": "skipped"
});
formatter.after({
  "duration": 428285,
  "error_message": "java.lang.NullPointerException\r\n\tat stepDefinitions.ChangePassword.tearDown(ChangePassword.java:178)\r\n\tat sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)\r\n\tat sun.reflect.NativeMethodAccessorImpl.invoke(NativeMethodAccessorImpl.java:62)\r\n\tat sun.reflect.DelegatingMethodAccessorImpl.invoke(DelegatingMethodAccessorImpl.java:43)\r\n\tat java.lang.reflect.Method.invoke(Method.java:498)\r\n\tat cucumber.runtime.Utils$1.call(Utils.java:40)\r\n\tat cucumber.runtime.Timeout.timeout(Timeout.java:16)\r\n\tat cucumber.runtime.Utils.invoke(Utils.java:34)\r\n\tat cucumber.runtime.java.JavaHookDefinition.execute(JavaHookDefinition.java:60)\r\n\tat cucumber.runtime.Runtime.runHookIfTagsMatch(Runtime.java:224)\r\n\tat cucumber.runtime.Runtime.runHooks(Runtime.java:212)\r\n\tat cucumber.runtime.Runtime.runAfterHooks(Runtime.java:206)\r\n\tat cucumber.runtime.model.CucumberScenario.run(CucumberScenario.java:46)\r\n\tat cucumber.runtime.junit.ExecutionUnitRunner.run(ExecutionUnitRunner.java:102)\r\n\tat org.junit.runners.Suite.runChild(Suite.java:127)\r\n\tat org.junit.runners.Suite.runChild(Suite.java:26)\r\n\tat org.junit.runners.ParentRunner$3.run(ParentRunner.java:238)\r\n\tat org.junit.runners.ParentRunner$1.schedule(ParentRunner.java:63)\r\n\tat org.junit.runners.ParentRunner.runChildren(ParentRunner.java:236)\r\n\tat org.junit.runners.ParentRunner.access$000(ParentRunner.java:53)\r\n\tat org.junit.runners.ParentRunner$2.evaluate(ParentRunner.java:229)\r\n\tat org.junit.runners.ParentRunner.run(ParentRunner.java:309)\r\n\tat cucumber.runtime.junit.ExamplesRunner.run(ExamplesRunner.java:59)\r\n\tat org.junit.runners.Suite.runChild(Suite.java:127)\r\n\tat org.junit.runners.Suite.runChild(Suite.java:26)\r\n\tat org.junit.runners.ParentRunner$3.run(ParentRunner.java:238)\r\n\tat org.junit.runners.ParentRunner$1.schedule(ParentRunner.java:63)\r\n\tat org.junit.runners.ParentRunner.runChildren(ParentRunner.java:236)\r\n\tat org.junit.runners.ParentRunner.access$000(ParentRunner.java:53)\r\n\tat org.junit.runners.ParentRunner$2.evaluate(ParentRunner.java:229)\r\n\tat org.junit.runners.ParentRunner.run(ParentRunner.java:309)\r\n\tat cucumber.runtime.junit.ScenarioOutlineRunner.run(ScenarioOutlineRunner.java:53)\r\n\tat cucumber.runtime.junit.FeatureRunner.runChild(FeatureRunner.java:63)\r\n\tat cucumber.runtime.junit.FeatureRunner.runChild(FeatureRunner.java:18)\r\n\tat org.junit.runners.ParentRunner$3.run(ParentRunner.java:238)\r\n\tat org.junit.runners.ParentRunner$1.schedule(ParentRunner.java:63)\r\n\tat org.junit.runners.ParentRunner.runChildren(ParentRunner.java:236)\r\n\tat org.junit.runners.ParentRunner.access$000(ParentRunner.java:53)\r\n\tat org.junit.runners.ParentRunner$2.evaluate(ParentRunner.java:229)\r\n\tat org.junit.runners.ParentRunner.run(ParentRunner.java:309)\r\n\tat cucumber.runtime.junit.FeatureRunner.run(FeatureRunner.java:70)\r\n\tat cucumber.api.junit.Cucumber.runChild(Cucumber.java:95)\r\n\tat cucumber.api.junit.Cucumber.runChild(Cucumber.java:38)\r\n\tat org.junit.runners.ParentRunner$3.run(ParentRunner.java:238)\r\n\tat org.junit.runners.ParentRunner$1.schedule(ParentRunner.java:63)\r\n\tat org.junit.runners.ParentRunner.runChildren(ParentRunner.java:236)\r\n\tat org.junit.runners.ParentRunner.access$000(ParentRunner.java:53)\r\n\tat org.junit.runners.ParentRunner$2.evaluate(ParentRunner.java:229)\r\n\tat org.junit.runners.ParentRunner.run(ParentRunner.java:309)\r\n\tat cucumber.api.junit.Cucumber.run(Cucumber.java:100)\r\n\tat org.apache.maven.surefire.junit4.JUnit4Provider.execute(JUnit4Provider.java:252)\r\n\tat org.apache.maven.surefire.junit4.JUnit4Provider.executeTestSet(JUnit4Provider.java:141)\r\n\tat org.apache.maven.surefire.junit4.JUnit4Provider.invoke(JUnit4Provider.java:112)\r\n\tat sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)\r\n\tat sun.reflect.NativeMethodAccessorImpl.invoke(NativeMethodAccessorImpl.java:62)\r\n\tat sun.reflect.DelegatingMethodAccessorImpl.invoke(DelegatingMethodAccessorImpl.java:43)\r\n\tat java.lang.reflect.Method.invoke(Method.java:498)\r\n\tat org.apache.maven.surefire.util.ReflectionUtils.invokeMethodWithArray(ReflectionUtils.java:189)\r\n\tat org.apache.maven.surefire.booter.ProviderFactory$ProviderProxy.invoke(ProviderFactory.java:165)\r\n\tat org.apache.maven.surefire.booter.ProviderFactory.invokeProvider(ProviderFactory.java:85)\r\n\tat org.apache.maven.surefire.booter.ForkedBooter.runSuitesInProcess(ForkedBooter.java:115)\r\n\tat org.apache.maven.surefire.booter.ForkedBooter.main(ForkedBooter.java:75)\r\n",
  "status": "failed"
});
formatter.embedding("image/png", "embedded0.png");
formatter.after({
  "duration": 491214234,
  "status": "passed"
});
formatter.after({
  "duration": 330786,
  "error_message": "java.lang.NullPointerException\r\n\tat stepDefinitions.Login.tearDown(Login.java:162)\r\n\tat sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)\r\n\tat sun.reflect.NativeMethodAccessorImpl.invoke(NativeMethodAccessorImpl.java:62)\r\n\tat sun.reflect.DelegatingMethodAccessorImpl.invoke(DelegatingMethodAccessorImpl.java:43)\r\n\tat java.lang.reflect.Method.invoke(Method.java:498)\r\n\tat cucumber.runtime.Utils$1.call(Utils.java:40)\r\n\tat cucumber.runtime.Timeout.timeout(Timeout.java:16)\r\n\tat cucumber.runtime.Utils.invoke(Utils.java:34)\r\n\tat cucumber.runtime.java.JavaHookDefinition.execute(JavaHookDefinition.java:60)\r\n\tat cucumber.runtime.Runtime.runHookIfTagsMatch(Runtime.java:224)\r\n\tat cucumber.runtime.Runtime.runHooks(Runtime.java:212)\r\n\tat cucumber.runtime.Runtime.runAfterHooks(Runtime.java:206)\r\n\tat cucumber.runtime.model.CucumberScenario.run(CucumberScenario.java:46)\r\n\tat cucumber.runtime.junit.ExecutionUnitRunner.run(ExecutionUnitRunner.java:102)\r\n\tat org.junit.runners.Suite.runChild(Suite.java:127)\r\n\tat org.junit.runners.Suite.runChild(Suite.java:26)\r\n\tat org.junit.runners.ParentRunner$3.run(ParentRunner.java:238)\r\n\tat org.junit.runners.ParentRunner$1.schedule(ParentRunner.java:63)\r\n\tat org.junit.runners.ParentRunner.runChildren(ParentRunner.java:236)\r\n\tat org.junit.runners.ParentRunner.access$000(ParentRunner.java:53)\r\n\tat org.junit.runners.ParentRunner$2.evaluate(ParentRunner.java:229)\r\n\tat org.junit.runners.ParentRunner.run(ParentRunner.java:309)\r\n\tat cucumber.runtime.junit.ExamplesRunner.run(ExamplesRunner.java:59)\r\n\tat org.junit.runners.Suite.runChild(Suite.java:127)\r\n\tat org.junit.runners.Suite.runChild(Suite.java:26)\r\n\tat org.junit.runners.ParentRunner$3.run(ParentRunner.java:238)\r\n\tat org.junit.runners.ParentRunner$1.schedule(ParentRunner.java:63)\r\n\tat org.junit.runners.ParentRunner.runChildren(ParentRunner.java:236)\r\n\tat org.junit.runners.ParentRunner.access$000(ParentRunner.java:53)\r\n\tat org.junit.runners.ParentRunner$2.evaluate(ParentRunner.java:229)\r\n\tat org.junit.runners.ParentRunner.run(ParentRunner.java:309)\r\n\tat cucumber.runtime.junit.ScenarioOutlineRunner.run(ScenarioOutlineRunner.java:53)\r\n\tat cucumber.runtime.junit.FeatureRunner.runChild(FeatureRunner.java:63)\r\n\tat cucumber.runtime.junit.FeatureRunner.runChild(FeatureRunner.java:18)\r\n\tat org.junit.runners.ParentRunner$3.run(ParentRunner.java:238)\r\n\tat org.junit.runners.ParentRunner$1.schedule(ParentRunner.java:63)\r\n\tat org.junit.runners.ParentRunner.runChildren(ParentRunner.java:236)\r\n\tat org.junit.runners.ParentRunner.access$000(ParentRunner.java:53)\r\n\tat org.junit.runners.ParentRunner$2.evaluate(ParentRunner.java:229)\r\n\tat org.junit.runners.ParentRunner.run(ParentRunner.java:309)\r\n\tat cucumber.runtime.junit.FeatureRunner.run(FeatureRunner.java:70)\r\n\tat cucumber.api.junit.Cucumber.runChild(Cucumber.java:95)\r\n\tat cucumber.api.junit.Cucumber.runChild(Cucumber.java:38)\r\n\tat org.junit.runners.ParentRunner$3.run(ParentRunner.java:238)\r\n\tat org.junit.runners.ParentRunner$1.schedule(ParentRunner.java:63)\r\n\tat org.junit.runners.ParentRunner.runChildren(ParentRunner.java:236)\r\n\tat org.junit.runners.ParentRunner.access$000(ParentRunner.java:53)\r\n\tat org.junit.runners.ParentRunner$2.evaluate(ParentRunner.java:229)\r\n\tat org.junit.runners.ParentRunner.run(ParentRunner.java:309)\r\n\tat cucumber.api.junit.Cucumber.run(Cucumber.java:100)\r\n\tat org.apache.maven.surefire.junit4.JUnit4Provider.execute(JUnit4Provider.java:252)\r\n\tat org.apache.maven.surefire.junit4.JUnit4Provider.executeTestSet(JUnit4Provider.java:141)\r\n\tat org.apache.maven.surefire.junit4.JUnit4Provider.invoke(JUnit4Provider.java:112)\r\n\tat sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)\r\n\tat sun.reflect.NativeMethodAccessorImpl.invoke(NativeMethodAccessorImpl.java:62)\r\n\tat sun.reflect.DelegatingMethodAccessorImpl.invoke(DelegatingMethodAccessorImpl.java:43)\r\n\tat java.lang.reflect.Method.invoke(Method.java:498)\r\n\tat org.apache.maven.surefire.util.ReflectionUtils.invokeMethodWithArray(ReflectionUtils.java:189)\r\n\tat org.apache.maven.surefire.booter.ProviderFactory$ProviderProxy.invoke(ProviderFactory.java:165)\r\n\tat org.apache.maven.surefire.booter.ProviderFactory.invokeProvider(ProviderFactory.java:85)\r\n\tat org.apache.maven.surefire.booter.ForkedBooter.runSuitesInProcess(ForkedBooter.java:115)\r\n\tat org.apache.maven.surefire.booter.ForkedBooter.main(ForkedBooter.java:75)\r\n",
  "status": "failed"
});
formatter.scenario({
  "line": 162,
  "name": "TestCaseId- \"IWEB_SZMK_279\" - To verify Order Input with \"incorrect limit price in SZMK\" rest",
  "description": "",
  "id": "ordering-stock;testcaseid--\"\u003ctestcaseid\u003e\"---to-verify-order-input-with-\"\u003cscenario\u003e\"-rest;;4",
  "type": "scenario",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "line": 144,
      "name": "@smoke2"
    },
    {
      "line": 144,
      "name": "@test2"
    }
  ]
});
formatter.step({
  "line": 146,
  "name": "user logged with \"username\" and \"password\" and submit",
  "keyword": "Given "
});
formatter.step({
  "line": 147,
  "name": "user click on buy icon",
  "keyword": "When "
});
formatter.step({
  "line": 148,
  "name": "user select type of market as \"SZMK\"",
  "matchedColumns": [
    1
  ],
  "keyword": "Then "
});
formatter.step({
  "line": 149,
  "name": "user enter stock code as \"2\"",
  "matchedColumns": [
    2
  ],
  "keyword": "And "
});
formatter.step({
  "line": 150,
  "name": "order price as \"23.000\"",
  "matchedColumns": [
    3
  ],
  "keyword": "And "
});
formatter.step({
  "line": 151,
  "name": "order quantity as \"1000\"",
  "matchedColumns": [
    4
  ],
  "keyword": "And "
});
formatter.step({
  "line": 152,
  "name": "order type as \"Limit Order\"",
  "matchedColumns": [
    5
  ],
  "keyword": "And "
});
formatter.step({
  "line": 153,
  "name": "order term as \"Good for today\"",
  "matchedColumns": [
    6
  ],
  "keyword": "And "
});
formatter.step({
  "line": 154,
  "name": "Preview order",
  "keyword": "Then "
});
formatter.step({
  "line": 155,
  "name": "message given is \"SZMK buy order price violates max 3% change limit. [1515]\"",
  "matchedColumns": [
    7
  ],
  "keyword": "Then "
});
formatter.step({
  "line": 156,
  "name": "close the current browser",
  "keyword": "Then "
});
formatter.match({
  "arguments": [
    {
      "val": "username",
      "offset": 18
    },
    {
      "val": "password",
      "offset": 33
    }
  ],
  "location": "OrderInputSD.user_logged_in_to_the_application(String,String)"
});
formatter.result({
  "duration": 10103408952,
  "status": "passed"
});
formatter.match({
  "location": "OrderInputSD.user_click_on_buy_icon()"
});
formatter.result({
  "duration": 9027643536,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "SZMK",
      "offset": 31
    }
  ],
  "location": "OrderInputSD.user_select_type_of_market_as(String)"
});
formatter.result({
  "duration": 275386552,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "2",
      "offset": 26
    }
  ],
  "location": "OrderInputSD.user_enter_stock_code_as(String)"
});
formatter.result({
  "duration": 214176909,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "23.000",
      "offset": 16
    }
  ],
  "location": "OrderInputSD.order_price_as(String)"
});
formatter.result({
  "duration": 436023477,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "1000",
      "offset": 19
    }
  ],
  "location": "OrderInputSD.order_quantity_as(String)"
});
formatter.result({
  "duration": 251515083,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Limit Order",
      "offset": 15
    }
  ],
  "location": "OrderInputSD.order_type_as(String)"
});
formatter.result({
  "duration": 263605751,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Good for today",
      "offset": 15
    }
  ],
  "location": "OrderInputSD.order_term_as(String)"
});
formatter.result({
  "duration": 269085037,
  "status": "passed"
});
formatter.match({
  "location": "OrderInputSD.preview_order()"
});
formatter.result({
  "duration": 287967083,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "SZMK buy order price violates max 3% change limit. [1515]",
      "offset": 18
    }
  ],
  "location": "OrderInputSD.error_message_given_is(String)"
});
formatter.result({
  "duration": 747575452,
  "error_message": "junit.framework.ComparisonFailure: expected:\u003c[SZMK buy order price violates max 3% change limit. [1515]]\u003e but was:\u003c[Do not allow to buy this product [10013]]\u003e\r\n\tat junit.framework.Assert.assertEquals(Assert.java:100)\r\n\tat junit.framework.Assert.assertEquals(Assert.java:107)\r\n\tat stepDefinitions.OrderInputSD.error_message_given_is(OrderInputSD.java:175)\r\n\tat ✽.Then message given is \"SZMK buy order price violates max 3% change limit. [1515]\"(OrderInput.feature:155)\r\n",
  "status": "failed"
});
formatter.match({
  "location": "OrderInputSD.close_the_current_browser()"
});
formatter.result({
  "status": "skipped"
});
formatter.after({
  "duration": 363154,
  "error_message": "java.lang.NullPointerException\r\n\tat stepDefinitions.ChangePassword.tearDown(ChangePassword.java:178)\r\n\tat sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)\r\n\tat sun.reflect.NativeMethodAccessorImpl.invoke(NativeMethodAccessorImpl.java:62)\r\n\tat sun.reflect.DelegatingMethodAccessorImpl.invoke(DelegatingMethodAccessorImpl.java:43)\r\n\tat java.lang.reflect.Method.invoke(Method.java:498)\r\n\tat cucumber.runtime.Utils$1.call(Utils.java:40)\r\n\tat cucumber.runtime.Timeout.timeout(Timeout.java:16)\r\n\tat cucumber.runtime.Utils.invoke(Utils.java:34)\r\n\tat cucumber.runtime.java.JavaHookDefinition.execute(JavaHookDefinition.java:60)\r\n\tat cucumber.runtime.Runtime.runHookIfTagsMatch(Runtime.java:224)\r\n\tat cucumber.runtime.Runtime.runHooks(Runtime.java:212)\r\n\tat cucumber.runtime.Runtime.runAfterHooks(Runtime.java:206)\r\n\tat cucumber.runtime.model.CucumberScenario.run(CucumberScenario.java:46)\r\n\tat cucumber.runtime.junit.ExecutionUnitRunner.run(ExecutionUnitRunner.java:102)\r\n\tat org.junit.runners.Suite.runChild(Suite.java:127)\r\n\tat org.junit.runners.Suite.runChild(Suite.java:26)\r\n\tat org.junit.runners.ParentRunner$3.run(ParentRunner.java:238)\r\n\tat org.junit.runners.ParentRunner$1.schedule(ParentRunner.java:63)\r\n\tat org.junit.runners.ParentRunner.runChildren(ParentRunner.java:236)\r\n\tat org.junit.runners.ParentRunner.access$000(ParentRunner.java:53)\r\n\tat org.junit.runners.ParentRunner$2.evaluate(ParentRunner.java:229)\r\n\tat org.junit.runners.ParentRunner.run(ParentRunner.java:309)\r\n\tat cucumber.runtime.junit.ExamplesRunner.run(ExamplesRunner.java:59)\r\n\tat org.junit.runners.Suite.runChild(Suite.java:127)\r\n\tat org.junit.runners.Suite.runChild(Suite.java:26)\r\n\tat org.junit.runners.ParentRunner$3.run(ParentRunner.java:238)\r\n\tat org.junit.runners.ParentRunner$1.schedule(ParentRunner.java:63)\r\n\tat org.junit.runners.ParentRunner.runChildren(ParentRunner.java:236)\r\n\tat org.junit.runners.ParentRunner.access$000(ParentRunner.java:53)\r\n\tat org.junit.runners.ParentRunner$2.evaluate(ParentRunner.java:229)\r\n\tat org.junit.runners.ParentRunner.run(ParentRunner.java:309)\r\n\tat cucumber.runtime.junit.ScenarioOutlineRunner.run(ScenarioOutlineRunner.java:53)\r\n\tat cucumber.runtime.junit.FeatureRunner.runChild(FeatureRunner.java:63)\r\n\tat cucumber.runtime.junit.FeatureRunner.runChild(FeatureRunner.java:18)\r\n\tat org.junit.runners.ParentRunner$3.run(ParentRunner.java:238)\r\n\tat org.junit.runners.ParentRunner$1.schedule(ParentRunner.java:63)\r\n\tat org.junit.runners.ParentRunner.runChildren(ParentRunner.java:236)\r\n\tat org.junit.runners.ParentRunner.access$000(ParentRunner.java:53)\r\n\tat org.junit.runners.ParentRunner$2.evaluate(ParentRunner.java:229)\r\n\tat org.junit.runners.ParentRunner.run(ParentRunner.java:309)\r\n\tat cucumber.runtime.junit.FeatureRunner.run(FeatureRunner.java:70)\r\n\tat cucumber.api.junit.Cucumber.runChild(Cucumber.java:95)\r\n\tat cucumber.api.junit.Cucumber.runChild(Cucumber.java:38)\r\n\tat org.junit.runners.ParentRunner$3.run(ParentRunner.java:238)\r\n\tat org.junit.runners.ParentRunner$1.schedule(ParentRunner.java:63)\r\n\tat org.junit.runners.ParentRunner.runChildren(ParentRunner.java:236)\r\n\tat org.junit.runners.ParentRunner.access$000(ParentRunner.java:53)\r\n\tat org.junit.runners.ParentRunner$2.evaluate(ParentRunner.java:229)\r\n\tat org.junit.runners.ParentRunner.run(ParentRunner.java:309)\r\n\tat cucumber.api.junit.Cucumber.run(Cucumber.java:100)\r\n\tat org.apache.maven.surefire.junit4.JUnit4Provider.execute(JUnit4Provider.java:252)\r\n\tat org.apache.maven.surefire.junit4.JUnit4Provider.executeTestSet(JUnit4Provider.java:141)\r\n\tat org.apache.maven.surefire.junit4.JUnit4Provider.invoke(JUnit4Provider.java:112)\r\n\tat sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)\r\n\tat sun.reflect.NativeMethodAccessorImpl.invoke(NativeMethodAccessorImpl.java:62)\r\n\tat sun.reflect.DelegatingMethodAccessorImpl.invoke(DelegatingMethodAccessorImpl.java:43)\r\n\tat java.lang.reflect.Method.invoke(Method.java:498)\r\n\tat org.apache.maven.surefire.util.ReflectionUtils.invokeMethodWithArray(ReflectionUtils.java:189)\r\n\tat org.apache.maven.surefire.booter.ProviderFactory$ProviderProxy.invoke(ProviderFactory.java:165)\r\n\tat org.apache.maven.surefire.booter.ProviderFactory.invokeProvider(ProviderFactory.java:85)\r\n\tat org.apache.maven.surefire.booter.ForkedBooter.runSuitesInProcess(ForkedBooter.java:115)\r\n\tat org.apache.maven.surefire.booter.ForkedBooter.main(ForkedBooter.java:75)\r\n",
  "status": "failed"
});
formatter.embedding("image/png", "embedded1.png");
formatter.after({
  "duration": 457994308,
  "status": "passed"
});
formatter.after({
  "duration": 458679,
  "error_message": "java.lang.NullPointerException\r\n\tat stepDefinitions.Login.tearDown(Login.java:162)\r\n\tat sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)\r\n\tat sun.reflect.NativeMethodAccessorImpl.invoke(NativeMethodAccessorImpl.java:62)\r\n\tat sun.reflect.DelegatingMethodAccessorImpl.invoke(DelegatingMethodAccessorImpl.java:43)\r\n\tat java.lang.reflect.Method.invoke(Method.java:498)\r\n\tat cucumber.runtime.Utils$1.call(Utils.java:40)\r\n\tat cucumber.runtime.Timeout.timeout(Timeout.java:16)\r\n\tat cucumber.runtime.Utils.invoke(Utils.java:34)\r\n\tat cucumber.runtime.java.JavaHookDefinition.execute(JavaHookDefinition.java:60)\r\n\tat cucumber.runtime.Runtime.runHookIfTagsMatch(Runtime.java:224)\r\n\tat cucumber.runtime.Runtime.runHooks(Runtime.java:212)\r\n\tat cucumber.runtime.Runtime.runAfterHooks(Runtime.java:206)\r\n\tat cucumber.runtime.model.CucumberScenario.run(CucumberScenario.java:46)\r\n\tat cucumber.runtime.junit.ExecutionUnitRunner.run(ExecutionUnitRunner.java:102)\r\n\tat org.junit.runners.Suite.runChild(Suite.java:127)\r\n\tat org.junit.runners.Suite.runChild(Suite.java:26)\r\n\tat org.junit.runners.ParentRunner$3.run(ParentRunner.java:238)\r\n\tat org.junit.runners.ParentRunner$1.schedule(ParentRunner.java:63)\r\n\tat org.junit.runners.ParentRunner.runChildren(ParentRunner.java:236)\r\n\tat org.junit.runners.ParentRunner.access$000(ParentRunner.java:53)\r\n\tat org.junit.runners.ParentRunner$2.evaluate(ParentRunner.java:229)\r\n\tat org.junit.runners.ParentRunner.run(ParentRunner.java:309)\r\n\tat cucumber.runtime.junit.ExamplesRunner.run(ExamplesRunner.java:59)\r\n\tat org.junit.runners.Suite.runChild(Suite.java:127)\r\n\tat org.junit.runners.Suite.runChild(Suite.java:26)\r\n\tat org.junit.runners.ParentRunner$3.run(ParentRunner.java:238)\r\n\tat org.junit.runners.ParentRunner$1.schedule(ParentRunner.java:63)\r\n\tat org.junit.runners.ParentRunner.runChildren(ParentRunner.java:236)\r\n\tat org.junit.runners.ParentRunner.access$000(ParentRunner.java:53)\r\n\tat org.junit.runners.ParentRunner$2.evaluate(ParentRunner.java:229)\r\n\tat org.junit.runners.ParentRunner.run(ParentRunner.java:309)\r\n\tat cucumber.runtime.junit.ScenarioOutlineRunner.run(ScenarioOutlineRunner.java:53)\r\n\tat cucumber.runtime.junit.FeatureRunner.runChild(FeatureRunner.java:63)\r\n\tat cucumber.runtime.junit.FeatureRunner.runChild(FeatureRunner.java:18)\r\n\tat org.junit.runners.ParentRunner$3.run(ParentRunner.java:238)\r\n\tat org.junit.runners.ParentRunner$1.schedule(ParentRunner.java:63)\r\n\tat org.junit.runners.ParentRunner.runChildren(ParentRunner.java:236)\r\n\tat org.junit.runners.ParentRunner.access$000(ParentRunner.java:53)\r\n\tat org.junit.runners.ParentRunner$2.evaluate(ParentRunner.java:229)\r\n\tat org.junit.runners.ParentRunner.run(ParentRunner.java:309)\r\n\tat cucumber.runtime.junit.FeatureRunner.run(FeatureRunner.java:70)\r\n\tat cucumber.api.junit.Cucumber.runChild(Cucumber.java:95)\r\n\tat cucumber.api.junit.Cucumber.runChild(Cucumber.java:38)\r\n\tat org.junit.runners.ParentRunner$3.run(ParentRunner.java:238)\r\n\tat org.junit.runners.ParentRunner$1.schedule(ParentRunner.java:63)\r\n\tat org.junit.runners.ParentRunner.runChildren(ParentRunner.java:236)\r\n\tat org.junit.runners.ParentRunner.access$000(ParentRunner.java:53)\r\n\tat org.junit.runners.ParentRunner$2.evaluate(ParentRunner.java:229)\r\n\tat org.junit.runners.ParentRunner.run(ParentRunner.java:309)\r\n\tat cucumber.api.junit.Cucumber.run(Cucumber.java:100)\r\n\tat org.apache.maven.surefire.junit4.JUnit4Provider.execute(JUnit4Provider.java:252)\r\n\tat org.apache.maven.surefire.junit4.JUnit4Provider.executeTestSet(JUnit4Provider.java:141)\r\n\tat org.apache.maven.surefire.junit4.JUnit4Provider.invoke(JUnit4Provider.java:112)\r\n\tat sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)\r\n\tat sun.reflect.NativeMethodAccessorImpl.invoke(NativeMethodAccessorImpl.java:62)\r\n\tat sun.reflect.DelegatingMethodAccessorImpl.invoke(DelegatingMethodAccessorImpl.java:43)\r\n\tat java.lang.reflect.Method.invoke(Method.java:498)\r\n\tat org.apache.maven.surefire.util.ReflectionUtils.invokeMethodWithArray(ReflectionUtils.java:189)\r\n\tat org.apache.maven.surefire.booter.ProviderFactory$ProviderProxy.invoke(ProviderFactory.java:165)\r\n\tat org.apache.maven.surefire.booter.ProviderFactory.invokeProvider(ProviderFactory.java:85)\r\n\tat org.apache.maven.surefire.booter.ForkedBooter.runSuitesInProcess(ForkedBooter.java:115)\r\n\tat org.apache.maven.surefire.booter.ForkedBooter.main(ForkedBooter.java:75)\r\n",
  "status": "failed"
});
formatter.scenario({
  "line": 163,
  "name": "TestCaseId- \"IWEB_SZMK_226\" - To verify Order Input with \"incorrect quantity for given stock code in SZMK\" rest",
  "description": "",
  "id": "ordering-stock;testcaseid--\"\u003ctestcaseid\u003e\"---to-verify-order-input-with-\"\u003cscenario\u003e\"-rest;;5",
  "type": "scenario",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "line": 144,
      "name": "@smoke2"
    },
    {
      "line": 144,
      "name": "@test2"
    }
  ]
});
formatter.step({
  "line": 146,
  "name": "user logged with \"username\" and \"password\" and submit",
  "keyword": "Given "
});
formatter.step({
  "line": 147,
  "name": "user click on buy icon",
  "keyword": "When "
});
formatter.step({
  "line": 148,
  "name": "user select type of market as \"SZMK\"",
  "matchedColumns": [
    1
  ],
  "keyword": "Then "
});
formatter.step({
  "line": 149,
  "name": "user enter stock code as \"8\"",
  "matchedColumns": [
    2
  ],
  "keyword": "And "
});
formatter.step({
  "line": 150,
  "name": "order price as \"4.400\"",
  "matchedColumns": [
    3
  ],
  "keyword": "And "
});
formatter.step({
  "line": 151,
  "name": "order quantity as \"1000\"",
  "matchedColumns": [
    4
  ],
  "keyword": "And "
});
formatter.step({
  "line": 152,
  "name": "order type as \"Limit Order\"",
  "matchedColumns": [
    5
  ],
  "keyword": "And "
});
formatter.step({
  "line": 153,
  "name": "order term as \"Good for today\"",
  "matchedColumns": [
    6
  ],
  "keyword": "And "
});
formatter.step({
  "line": 154,
  "name": "Preview order",
  "keyword": "Then "
});
formatter.step({
  "line": 155,
  "name": "message given is \"SZMK buy order price violates max 3% change limit. [1515]\"",
  "matchedColumns": [
    7
  ],
  "keyword": "Then "
});
formatter.step({
  "line": 156,
  "name": "close the current browser",
  "keyword": "Then "
});
formatter.match({
  "arguments": [
    {
      "val": "username",
      "offset": 18
    },
    {
      "val": "password",
      "offset": 33
    }
  ],
  "location": "OrderInputSD.user_logged_in_to_the_application(String,String)"
});
formatter.result({
  "duration": 10261785242,
  "status": "passed"
});
formatter.match({
  "location": "OrderInputSD.user_click_on_buy_icon()"
});
formatter.result({
  "duration": 9271691458,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "SZMK",
      "offset": 31
    }
  ],
  "location": "OrderInputSD.user_select_type_of_market_as(String)"
});
formatter.result({
  "duration": 287670244,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "8",
      "offset": 26
    }
  ],
  "location": "OrderInputSD.user_enter_stock_code_as(String)"
});
formatter.result({
  "duration": 245947376,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "4.400",
      "offset": 16
    }
  ],
  "location": "OrderInputSD.order_price_as(String)"
});
formatter.result({
  "duration": 458051939,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "1000",
      "offset": 19
    }
  ],
  "location": "OrderInputSD.order_quantity_as(String)"
});
formatter.result({
  "duration": 300724848,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Limit Order",
      "offset": 15
    }
  ],
  "location": "OrderInputSD.order_type_as(String)"
});
formatter.result({
  "duration": 264967184,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Good for today",
      "offset": 15
    }
  ],
  "location": "OrderInputSD.order_term_as(String)"
});
formatter.result({
  "duration": 303316269,
  "status": "passed"
});
formatter.match({
  "location": "OrderInputSD.preview_order()"
});
formatter.result({
  "duration": 280268608,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "SZMK buy order price violates max 3% change limit. [1515]",
      "offset": 18
    }
  ],
  "location": "OrderInputSD.error_message_given_is(String)"
});
formatter.result({
  "duration": 928920851,
  "error_message": "junit.framework.ComparisonFailure: expected:\u003c[SZMK buy order price violates max 3% change limit. [1515]]\u003e but was:\u003c[Do not allow to buy this product [10013]]\u003e\r\n\tat junit.framework.Assert.assertEquals(Assert.java:100)\r\n\tat junit.framework.Assert.assertEquals(Assert.java:107)\r\n\tat stepDefinitions.OrderInputSD.error_message_given_is(OrderInputSD.java:175)\r\n\tat ✽.Then message given is \"SZMK buy order price violates max 3% change limit. [1515]\"(OrderInput.feature:155)\r\n",
  "status": "failed"
});
formatter.match({
  "location": "OrderInputSD.close_the_current_browser()"
});
formatter.result({
  "status": "skipped"
});
formatter.after({
  "duration": 369075,
  "error_message": "java.lang.NullPointerException\r\n\tat stepDefinitions.ChangePassword.tearDown(ChangePassword.java:178)\r\n\tat sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)\r\n\tat sun.reflect.NativeMethodAccessorImpl.invoke(NativeMethodAccessorImpl.java:62)\r\n\tat sun.reflect.DelegatingMethodAccessorImpl.invoke(DelegatingMethodAccessorImpl.java:43)\r\n\tat java.lang.reflect.Method.invoke(Method.java:498)\r\n\tat cucumber.runtime.Utils$1.call(Utils.java:40)\r\n\tat cucumber.runtime.Timeout.timeout(Timeout.java:16)\r\n\tat cucumber.runtime.Utils.invoke(Utils.java:34)\r\n\tat cucumber.runtime.java.JavaHookDefinition.execute(JavaHookDefinition.java:60)\r\n\tat cucumber.runtime.Runtime.runHookIfTagsMatch(Runtime.java:224)\r\n\tat cucumber.runtime.Runtime.runHooks(Runtime.java:212)\r\n\tat cucumber.runtime.Runtime.runAfterHooks(Runtime.java:206)\r\n\tat cucumber.runtime.model.CucumberScenario.run(CucumberScenario.java:46)\r\n\tat cucumber.runtime.junit.ExecutionUnitRunner.run(ExecutionUnitRunner.java:102)\r\n\tat org.junit.runners.Suite.runChild(Suite.java:127)\r\n\tat org.junit.runners.Suite.runChild(Suite.java:26)\r\n\tat org.junit.runners.ParentRunner$3.run(ParentRunner.java:238)\r\n\tat org.junit.runners.ParentRunner$1.schedule(ParentRunner.java:63)\r\n\tat org.junit.runners.ParentRunner.runChildren(ParentRunner.java:236)\r\n\tat org.junit.runners.ParentRunner.access$000(ParentRunner.java:53)\r\n\tat org.junit.runners.ParentRunner$2.evaluate(ParentRunner.java:229)\r\n\tat org.junit.runners.ParentRunner.run(ParentRunner.java:309)\r\n\tat cucumber.runtime.junit.ExamplesRunner.run(ExamplesRunner.java:59)\r\n\tat org.junit.runners.Suite.runChild(Suite.java:127)\r\n\tat org.junit.runners.Suite.runChild(Suite.java:26)\r\n\tat org.junit.runners.ParentRunner$3.run(ParentRunner.java:238)\r\n\tat org.junit.runners.ParentRunner$1.schedule(ParentRunner.java:63)\r\n\tat org.junit.runners.ParentRunner.runChildren(ParentRunner.java:236)\r\n\tat org.junit.runners.ParentRunner.access$000(ParentRunner.java:53)\r\n\tat org.junit.runners.ParentRunner$2.evaluate(ParentRunner.java:229)\r\n\tat org.junit.runners.ParentRunner.run(ParentRunner.java:309)\r\n\tat cucumber.runtime.junit.ScenarioOutlineRunner.run(ScenarioOutlineRunner.java:53)\r\n\tat cucumber.runtime.junit.FeatureRunner.runChild(FeatureRunner.java:63)\r\n\tat cucumber.runtime.junit.FeatureRunner.runChild(FeatureRunner.java:18)\r\n\tat org.junit.runners.ParentRunner$3.run(ParentRunner.java:238)\r\n\tat org.junit.runners.ParentRunner$1.schedule(ParentRunner.java:63)\r\n\tat org.junit.runners.ParentRunner.runChildren(ParentRunner.java:236)\r\n\tat org.junit.runners.ParentRunner.access$000(ParentRunner.java:53)\r\n\tat org.junit.runners.ParentRunner$2.evaluate(ParentRunner.java:229)\r\n\tat org.junit.runners.ParentRunner.run(ParentRunner.java:309)\r\n\tat cucumber.runtime.junit.FeatureRunner.run(FeatureRunner.java:70)\r\n\tat cucumber.api.junit.Cucumber.runChild(Cucumber.java:95)\r\n\tat cucumber.api.junit.Cucumber.runChild(Cucumber.java:38)\r\n\tat org.junit.runners.ParentRunner$3.run(ParentRunner.java:238)\r\n\tat org.junit.runners.ParentRunner$1.schedule(ParentRunner.java:63)\r\n\tat org.junit.runners.ParentRunner.runChildren(ParentRunner.java:236)\r\n\tat org.junit.runners.ParentRunner.access$000(ParentRunner.java:53)\r\n\tat org.junit.runners.ParentRunner$2.evaluate(ParentRunner.java:229)\r\n\tat org.junit.runners.ParentRunner.run(ParentRunner.java:309)\r\n\tat cucumber.api.junit.Cucumber.run(Cucumber.java:100)\r\n\tat org.apache.maven.surefire.junit4.JUnit4Provider.execute(JUnit4Provider.java:252)\r\n\tat org.apache.maven.surefire.junit4.JUnit4Provider.executeTestSet(JUnit4Provider.java:141)\r\n\tat org.apache.maven.surefire.junit4.JUnit4Provider.invoke(JUnit4Provider.java:112)\r\n\tat sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)\r\n\tat sun.reflect.NativeMethodAccessorImpl.invoke(NativeMethodAccessorImpl.java:62)\r\n\tat sun.reflect.DelegatingMethodAccessorImpl.invoke(DelegatingMethodAccessorImpl.java:43)\r\n\tat java.lang.reflect.Method.invoke(Method.java:498)\r\n\tat org.apache.maven.surefire.util.ReflectionUtils.invokeMethodWithArray(ReflectionUtils.java:189)\r\n\tat org.apache.maven.surefire.booter.ProviderFactory$ProviderProxy.invoke(ProviderFactory.java:165)\r\n\tat org.apache.maven.surefire.booter.ProviderFactory.invokeProvider(ProviderFactory.java:85)\r\n\tat org.apache.maven.surefire.booter.ForkedBooter.runSuitesInProcess(ForkedBooter.java:115)\r\n\tat org.apache.maven.surefire.booter.ForkedBooter.main(ForkedBooter.java:75)\r\n",
  "status": "failed"
});
formatter.embedding("image/png", "embedded2.png");
formatter.after({
  "duration": 558019610,
  "status": "passed"
});
formatter.after({
  "duration": 479205,
  "error_message": "java.lang.NullPointerException\r\n\tat stepDefinitions.Login.tearDown(Login.java:162)\r\n\tat sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)\r\n\tat sun.reflect.NativeMethodAccessorImpl.invoke(NativeMethodAccessorImpl.java:62)\r\n\tat sun.reflect.DelegatingMethodAccessorImpl.invoke(DelegatingMethodAccessorImpl.java:43)\r\n\tat java.lang.reflect.Method.invoke(Method.java:498)\r\n\tat cucumber.runtime.Utils$1.call(Utils.java:40)\r\n\tat cucumber.runtime.Timeout.timeout(Timeout.java:16)\r\n\tat cucumber.runtime.Utils.invoke(Utils.java:34)\r\n\tat cucumber.runtime.java.JavaHookDefinition.execute(JavaHookDefinition.java:60)\r\n\tat cucumber.runtime.Runtime.runHookIfTagsMatch(Runtime.java:224)\r\n\tat cucumber.runtime.Runtime.runHooks(Runtime.java:212)\r\n\tat cucumber.runtime.Runtime.runAfterHooks(Runtime.java:206)\r\n\tat cucumber.runtime.model.CucumberScenario.run(CucumberScenario.java:46)\r\n\tat cucumber.runtime.junit.ExecutionUnitRunner.run(ExecutionUnitRunner.java:102)\r\n\tat org.junit.runners.Suite.runChild(Suite.java:127)\r\n\tat org.junit.runners.Suite.runChild(Suite.java:26)\r\n\tat org.junit.runners.ParentRunner$3.run(ParentRunner.java:238)\r\n\tat org.junit.runners.ParentRunner$1.schedule(ParentRunner.java:63)\r\n\tat org.junit.runners.ParentRunner.runChildren(ParentRunner.java:236)\r\n\tat org.junit.runners.ParentRunner.access$000(ParentRunner.java:53)\r\n\tat org.junit.runners.ParentRunner$2.evaluate(ParentRunner.java:229)\r\n\tat org.junit.runners.ParentRunner.run(ParentRunner.java:309)\r\n\tat cucumber.runtime.junit.ExamplesRunner.run(ExamplesRunner.java:59)\r\n\tat org.junit.runners.Suite.runChild(Suite.java:127)\r\n\tat org.junit.runners.Suite.runChild(Suite.java:26)\r\n\tat org.junit.runners.ParentRunner$3.run(ParentRunner.java:238)\r\n\tat org.junit.runners.ParentRunner$1.schedule(ParentRunner.java:63)\r\n\tat org.junit.runners.ParentRunner.runChildren(ParentRunner.java:236)\r\n\tat org.junit.runners.ParentRunner.access$000(ParentRunner.java:53)\r\n\tat org.junit.runners.ParentRunner$2.evaluate(ParentRunner.java:229)\r\n\tat org.junit.runners.ParentRunner.run(ParentRunner.java:309)\r\n\tat cucumber.runtime.junit.ScenarioOutlineRunner.run(ScenarioOutlineRunner.java:53)\r\n\tat cucumber.runtime.junit.FeatureRunner.runChild(FeatureRunner.java:63)\r\n\tat cucumber.runtime.junit.FeatureRunner.runChild(FeatureRunner.java:18)\r\n\tat org.junit.runners.ParentRunner$3.run(ParentRunner.java:238)\r\n\tat org.junit.runners.ParentRunner$1.schedule(ParentRunner.java:63)\r\n\tat org.junit.runners.ParentRunner.runChildren(ParentRunner.java:236)\r\n\tat org.junit.runners.ParentRunner.access$000(ParentRunner.java:53)\r\n\tat org.junit.runners.ParentRunner$2.evaluate(ParentRunner.java:229)\r\n\tat org.junit.runners.ParentRunner.run(ParentRunner.java:309)\r\n\tat cucumber.runtime.junit.FeatureRunner.run(FeatureRunner.java:70)\r\n\tat cucumber.api.junit.Cucumber.runChild(Cucumber.java:95)\r\n\tat cucumber.api.junit.Cucumber.runChild(Cucumber.java:38)\r\n\tat org.junit.runners.ParentRunner$3.run(ParentRunner.java:238)\r\n\tat org.junit.runners.ParentRunner$1.schedule(ParentRunner.java:63)\r\n\tat org.junit.runners.ParentRunner.runChildren(ParentRunner.java:236)\r\n\tat org.junit.runners.ParentRunner.access$000(ParentRunner.java:53)\r\n\tat org.junit.runners.ParentRunner$2.evaluate(ParentRunner.java:229)\r\n\tat org.junit.runners.ParentRunner.run(ParentRunner.java:309)\r\n\tat cucumber.api.junit.Cucumber.run(Cucumber.java:100)\r\n\tat org.apache.maven.surefire.junit4.JUnit4Provider.execute(JUnit4Provider.java:252)\r\n\tat org.apache.maven.surefire.junit4.JUnit4Provider.executeTestSet(JUnit4Provider.java:141)\r\n\tat org.apache.maven.surefire.junit4.JUnit4Provider.invoke(JUnit4Provider.java:112)\r\n\tat sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)\r\n\tat sun.reflect.NativeMethodAccessorImpl.invoke(NativeMethodAccessorImpl.java:62)\r\n\tat sun.reflect.DelegatingMethodAccessorImpl.invoke(DelegatingMethodAccessorImpl.java:43)\r\n\tat java.lang.reflect.Method.invoke(Method.java:498)\r\n\tat org.apache.maven.surefire.util.ReflectionUtils.invokeMethodWithArray(ReflectionUtils.java:189)\r\n\tat org.apache.maven.surefire.booter.ProviderFactory$ProviderProxy.invoke(ProviderFactory.java:165)\r\n\tat org.apache.maven.surefire.booter.ProviderFactory.invokeProvider(ProviderFactory.java:85)\r\n\tat org.apache.maven.surefire.booter.ForkedBooter.runSuitesInProcess(ForkedBooter.java:115)\r\n\tat org.apache.maven.surefire.booter.ForkedBooter.main(ForkedBooter.java:75)\r\n",
  "status": "failed"
});
formatter.scenario({
  "line": 164,
  "name": "TestCaseId- \"IWEB_SZMK_228\" - To verify Order Input with \"invalid stock code in SZMK\" rest",
  "description": "",
  "id": "ordering-stock;testcaseid--\"\u003ctestcaseid\u003e\"---to-verify-order-input-with-\"\u003cscenario\u003e\"-rest;;6",
  "type": "scenario",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "line": 144,
      "name": "@smoke2"
    },
    {
      "line": 144,
      "name": "@test2"
    }
  ]
});
formatter.step({
  "line": 146,
  "name": "user logged with \"username\" and \"password\" and submit",
  "keyword": "Given "
});
formatter.step({
  "line": 147,
  "name": "user click on buy icon",
  "keyword": "When "
});
formatter.step({
  "line": 148,
  "name": "user select type of market as \"SZMK\"",
  "matchedColumns": [
    1
  ],
  "keyword": "Then "
});
formatter.step({
  "line": 149,
  "name": "user enter stock code as \"00052\"",
  "matchedColumns": [
    2
  ],
  "keyword": "And "
});
formatter.step({
  "line": 150,
  "name": "order price as \"11.310\"",
  "matchedColumns": [
    3
  ],
  "keyword": "And "
});
formatter.step({
  "line": 151,
  "name": "order quantity as \"500\"",
  "matchedColumns": [
    4
  ],
  "keyword": "And "
});
formatter.step({
  "line": 152,
  "name": "order type as \"Limit Order\"",
  "matchedColumns": [
    5
  ],
  "keyword": "And "
});
formatter.step({
  "line": 153,
  "name": "order term as \"Good for today\"",
  "matchedColumns": [
    6
  ],
  "keyword": "And "
});
formatter.step({
  "line": 154,
  "name": "Preview order",
  "keyword": "Then "
});
formatter.step({
  "line": 155,
  "name": "message given is \"Stock code 00052 is invalid.\"",
  "matchedColumns": [
    7
  ],
  "keyword": "Then "
});
formatter.step({
  "line": 156,
  "name": "close the current browser",
  "keyword": "Then "
});
formatter.match({
  "arguments": [
    {
      "val": "username",
      "offset": 18
    },
    {
      "val": "password",
      "offset": 33
    }
  ],
  "location": "OrderInputSD.user_logged_in_to_the_application(String,String)"
});
formatter.result({
  "duration": 13812299007,
  "status": "passed"
});
formatter.match({
  "location": "OrderInputSD.user_click_on_buy_icon()"
});
formatter.result({
  "duration": 12412799370,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "SZMK",
      "offset": 31
    }
  ],
  "location": "OrderInputSD.user_select_type_of_market_as(String)"
});
formatter.result({
  "duration": 297355803,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "00052",
      "offset": 26
    }
  ],
  "location": "OrderInputSD.user_enter_stock_code_as(String)"
});
formatter.result({
  "duration": 347316349,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "11.310",
      "offset": 16
    }
  ],
  "location": "OrderInputSD.order_price_as(String)"
});
formatter.result({
  "duration": 511506659,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "500",
      "offset": 19
    }
  ],
  "location": "OrderInputSD.order_quantity_as(String)"
});
formatter.result({
  "duration": 282792530,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Limit Order",
      "offset": 15
    }
  ],
  "location": "OrderInputSD.order_type_as(String)"
});
formatter.result({
  "duration": 290916920,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Good for today",
      "offset": 15
    }
  ],
  "location": "OrderInputSD.order_term_as(String)"
});
formatter.result({
  "duration": 315235621,
  "status": "passed"
});
formatter.match({
  "location": "OrderInputSD.preview_order()"
});
formatter.result({
  "duration": 312704595,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Stock code 00052 is invalid.",
      "offset": 18
    }
  ],
  "location": "OrderInputSD.error_message_given_is(String)"
});
formatter.result({
  "duration": 763110558,
  "status": "passed"
});
formatter.match({
  "location": "OrderInputSD.close_the_current_browser()"
});
formatter.result({
  "duration": 818863019,
  "status": "passed"
});
formatter.after({
  "duration": 177235,
  "status": "passed"
});
formatter.after({
  "duration": 46579,
  "status": "passed"
});
formatter.after({
  "duration": 137762,
  "status": "passed"
});
});